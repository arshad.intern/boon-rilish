<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
			<div class="well rg_form">
				<center><h3>FOOD GALLERY<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3></center>
				<hr/>
			<div class="w3layouts_gallery_grids">	
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/idli2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/idli2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>IDLI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/rice bath2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/rice bath2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>RICE BATH</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/sweet-pongal2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/sweet-pongal2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>SWEET PONGAL</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/rava idli2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/rava idli2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>RAVA IDLI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/Masala Dosa2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/Masala Dosa2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>MASALA DOSA</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/Chattambade2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/Chattambade2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>CHATTAMBADE</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/biscut_rotti2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/biscut_rotti2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>BISCUT ROTTI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/bonda2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/bonda2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>POTATO BONDA</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/buns2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/buns2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>BUNS BHAJI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/puri-sagu2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/puri-sagu2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>PURI SAGU</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/palav2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/palav2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>PALAV</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/cheese-corn-balls2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/cheese-corn-balls2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>CHEESE CORN BALLS</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/Onion-Bhaji2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/Onion-Bhaji2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>ONION BHAJJI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/chilli-podi2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/chilli-podi2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>CHILLI PODI</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/veg-roll2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/veg-roll2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>VEG ROLL</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-3 w3layouts_gallery_grid">
					<a href="images/sandwich2.jpg" class="lsb-preview" data-lsb-group="header">
						<div class="w3layouts_news_grid">
							<img src="images/sandwich2.jpg" alt=" " class="img-responsive">
							<div class="w3layouts_news_grid_pos">
								<div class="wthree_text"><h3>SANDWICH</h3></div>
							</div>
						</div>
					</a>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		</div>
</div>
    <!-- /.container -->

    <!--<!-- gallery js -->
    <script src="js/lsb.min.js"></script>
    <script>
    $(window).load(function() {
          $.fn.lightspeedBox();
        });
    </script>
	</div>
</div>
    <!-- /.container -->

 
    <?php ///include("inc/footer.php"); ?>
    

</body>

</html>

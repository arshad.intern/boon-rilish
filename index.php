<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
		
<!-- //banner -->	
<?php 
ob_start();
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>BOON RELISH FOODS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Fresh Food a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
<!-- //Default-JavaScript-File -->

<link rel="stylesheet" href="css/easy-responsive-tabs.css"><!-- easy responsive tabs css -->

<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="all"><!-- date-picker css-->

<!-- gallery -->
	<link href="css/lsb.css" rel="stylesheet" type="text/css">
<!-- //gallery -->

<!-- banner text effect css files -->
    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style3.css" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
<!-- banner text effect css files -->

<!-- logo text effect css files -->
	<link rel="stylesheet" type="text/css" href="css/demo1.css" />
	<link rel="stylesheet" type="text/css" href="css/linkstyles.css" />
<!-- //logo text effect css files -->
	
<!-- default css files -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/font-awesome.min.css" />
<!-- default css files -->
	
<!--web font-->
	<link href="//fonts.googleapis.com/css?family=Pacifico&amp;subset=latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
<!--//web font-->
	
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->

</head>

<!-- Body -->
<body>

<div class="banner jarallax">
        <div class="agileinfo-dot">
            <div class="header">
                <div class="container-fluid">
                    <div class="header-left">
                        <div class="w3layouts-logo grid__item">
                            <h1>
                                <a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
                            </h1>
                        </div>
                    </div>
                    <div class="top-nav">
                        <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                	<ul class="nav navbar-nav">
                                	</ul>
                            	</div>
                            <div class="clearfix"> </div> 
   						</nav>      
                </div>      
            </div>
        </div>

<div class="container">
	<div class="row"><br>
		<div class="col-md-6 col-md-offset-3" style=" margin-top:10%; margin-bottom: 11%;">
			<div class="well login_form">
				<center><h2>LOGIN</h2></center>
				<hr/>
				<form method="post" action="">
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" data-validation="required email" placeholder="Enter Your Email" required/>
					</div>
			
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" data-validation="required" placeholder="Enter Your Password" required>
					</div>
					
					<div class="form-group">
						<input type="submit" name="log_btn" class="btn btn-primary btn-block" value="Login">
					</div>

							<div class="forgot-grid">
								<div class="forgot">
									<a href="forgotpassword.php">forgot password?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
				</form>
				<?php 
					if(isset($_POST['log_btn']))
					{
						$email = $_POST['email'];
						$password = $_POST['password'];

						include("admin/connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `users` WHERE email = '$email' AND password = '$password'") or die(mysqli_error($con));
						$row = mysqli_fetch_array($sql);
						if($row)
						{
							$_SESSION['email'] = $row['email'];
							header("location:home.php?logged_in");
						}
						else
						{
							echo '<h4 style="color:red; text-align:center;">Invalid Email and Password...</h4>';
						}
					}
					
				?>
				<p>Didn't Register Yet? <a href="user_reg.php">Register Now </a></p>
			</div>
		</div>
	</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>


</body>
<!-- //Body -->
</html>
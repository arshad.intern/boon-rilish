<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Feedback Form</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
		<div class="col-md-9 col-md-offset-2" style=" margin-top:5%; margin:bottom:10%; ">
			<div class="well rg_form">
				<center><h3>FEEDBACK FORM <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></center>
				<hr/>
				<form method="POST" action="feedback_val.php">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Feedback Sent Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while sending feedback.....!</p>
						</div>';
					}
				?>


					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="name">Customer Name</label>
								<input type="text" id="name" name="name" class="form-control" data-validation="required" placeholder="Enter Your Name" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Email ID</label>
								<input type="email" name="email" class="form-control" data-validation="required email" placeholder="Enter Your Email" required/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Phone Number</label>
							<input type="text" class="form-control" id="extra7" data-validation="required number length" data-validation-length="max10" name="number" placeholder="Enter Your Phone Number" onkeypress="return isNumber(event)" required="" />
					</div>
			
					<div class="form-group">
						<label>Message</label>
						<textarea rows="2" name="message" class="form-control" data-validation="required" placeholder="Enter Your Message" required></textarea>
					</div>
					
					<div class="form-group">
						<input type="submit" name="send_mess" class="btn btn-primary btn-block" value="SEND">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
    <!-- /.container -->

<!-- gallery js -->
    <script src="js/lsb.min.js"></script>
    <script>
    $(window).load(function() {
          $.fn.lightspeedBox();
        });
    </script>
	</div>
</div>
    <!-- /.container -->

 	<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   	<script src="validator.js">
   	NPM
   	$npm i jquery-form-validator

   	BOWER

	$bower install jquery-form-validator
	</script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<script>
  		$.validate({
    	lang: 'en'
  		});
	</script>
	<script>
			function allLetter(name)
			{
				var letters=/^[A-Za-z]+$/;
				if(name.value.match(letters))
				{
					return true;
				}
				else
				{
					alert('Name must contain alphabets only');
					name.focus();
					return false;
				}
			}
		</script>

    <?php include("inc/bottom.php"); ?>

    <script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
			function isNumber(evt) 
			{
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57))
			    {
			        return false;
    			}
   				 return true;
			}

		</script>
    

</body>

</html>

<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Food Ordering Form</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

</head>

<body>
	<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
		<div class="col-md-12">
			<div class="well rg_form">
				<center><h3>ORDER YOUR FOOD HERE <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3></center>
				<hr/>

				<div class="row">
				<form method="POST" action="order_food_val.php">
						<div class="col-md-4">
							<div class="form-group">
								<label>Order Number</label>
								<?php
									include("admin/connection.php");
								?>
								<input type="text" name="number" class="form-control" data-validation="required text" value="<?php echo  $_SESSION['order_no']; ?>" readonly/>
							</div>
					
							<div class="form-group">
								<label>Coupon Code (Optional)</label>
								<input type="code" name="code" class="form-control" placeholder="Enter Your Coupon Code" optional/>
							</div>

							<div class="form-group">
								<label>Order Date</label>
								<input type="date" name="date" data-validation="required date"	class="form-control" min="<?php echo date('Y-m-d'); ?>" placeholder="Enter your order date"/>				
							</div>
					
							<div class="form-group">
								<label>Customer Name</label>
								<input type="text" name="name" value="<?php echo $user_name; ?>" id="name" class="form-control" data-validation="required" placeholder="Enter Your Name" readonly="" required />
							</div>
							
						
							<div class="form-group">
								<label>Item Category</label>
								<select name="category" id="category" data-validation="required" class="form-control" required="">
										<option value="">--SELECT--</option>
										<?php
											include("admin/connection.php");
											$sql = mysqli_query($con, "SELECT * FROM `category` WHERE cat_type = 'menu'") or die(mysqli_error($con));
											while($row = mysqli_fetch_array($sql))
											{
												echo '<option value="'.$row['cat_id'].'">'.$row['cat_name'].'</option>';
											}
										?>
								</select>
							</div>

							<div class="form-group">
								<label>Selct food Item</label>
								<select name="item_name" id="item_name" data-validation="required" class="form-control" required="">
								</select>
							</div>
		
							<div class="form-group">
								<label>Unit Price</label>
								<input type="text" name="unit_price" class="form-control" id="unitprice" readonly />
							</div>

							<div class="form-group">
								<label>Remaining quantity</label>
								<input type="text"  name="qty"  class="form-control" id="remaining_item"  readonly />
							</div>

							<div class="form-group">
								<label>Required Quantity</label>
								<input type="text" name="r_qty"  id="quantity" data-validation="required number" class="form-control" placeholder="Enter the required quantity" required />
							</div>

							<div class="form-group">
								<label>Total Price</label>
							</div>
					
							<div class="form-group">
								<input type="text" id="total" readonly=""  name="total" class="form-control" required="" />
							</div>
					
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="submit" name="next" class="btn btn-primary btn-block" value="ADD NEXT ITEM">
									</div>
								</div>

								<div class="col-md-6">
										<div class="form-group">
										  <a href="select_pay.php" class="btn btn-primary btn-block">PROCEED</a>
										</div>
								</div>
							</div>
						</div>
					</form>
		

			<div class="col-md-8">
				<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Order Number</th>
								<th>Coupon Code</th>
								<th>Order Date</th>
								<th>Custoomer Name</th>
								<th>Item Category</th>
								<th>Item Name</th>
								<th>Unit Price</th>
								<th>Required Quantity</th>
								<th>Total Price</th>
								
							</tr>
						</thead>

						<tbody>

						<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `order` INNER JOIN `category` ON `order`.i_category = `category`.cat_id  WHERE `order`.o_number = '".$_SESSION['order_no']."'") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['o_number'].'</td>
							<td>'.$row['c_code'].'</td>
							<td>'.$row['o_date'].'</td>
							<td>'.$row['cust_name'].'</td>
							<td>'.$row['cat_name'].'</td>
							<td>'.$row['item_name'].'</td>
							<td>'.$row['unitprice'].'</td>
							<td>'.$row['req_qty'].'</td>
							<td>'.$row['total'].'</td>
							
							</tr>';
						}
					
					?>

						</tbody>
					</table>
				</div>
		</div>
	</span>
</h3>
</center>
</div>
</div>
</div>
</div>



    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>


    <script type="text/javascript">
$(document).ready(function(){
    $('#category').on('change',function(){
        var cat_id = $(this).val();
        //alert(cat_id);
        if(cat_id){
            $.ajax({
                type:'POST',
                url:'fetch_food.php',
                data:'cat_id='+cat_id,
                success:function(html){
                   // alert(html);
                    $('#item_name').html(html);
                },
                 error: function (error) {
                  
                }
            }); 
        }else{
            $('#item_name').html('<option value="">Select Category First</option>');
        }
    });


     $('#item_name').on('change',function(){
        var item_name = $(this).val();
        //alert(cat_id);
        if(item_name){
            $.ajax({
                type:'POST',
                url:'fetch_food_price.php',
                data:'item_name='+item_name,
                success:function(html){
                    //alert(html);
                    //$('#item_name').html(html);
                    $('#unitprice').val(html.unit_price);
                    $('#remaining_item').val(html.items_prepared);
                },
                 error: function (error) {
                  
                }
            }); 
        }else{
           // $('#item_name').html('<option value="">Select Category First</option>');
        }
    });
 });
</script>


<script>
$(function(){
    $("#quantity").on("change paste", function() {
        var req_qty = $(this).val();
        var unitprice = $("#unitprice").val();
        var remaining_item = $("#remaining_item").val();

        if(parseInt(req_qty) > parseInt(remaining_item))
        {
        	$("#total").val("");
        	$("#quantity").val("");
        	alert("Choose Required Quantity Less than "+remaining_item);
        }
        else
        {
        	 var t = unitprice * req_qty;
        	//alert(t);
        	$("#total").val(t);
        }
       
   
        })
});
</script>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>
</body>

</html>

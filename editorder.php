<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu2.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
<div class="container">
	<div class="row"><br>
			<div class="well rg_form">
				<center><h3>ORDER CONFIRMATION<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3></center>
				<hr/>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<?php
					if(isset($_GET['update']))
					{

						include("admin/connection.php");
						$id=$_GET['id'];
						$sql=mysqli_query($con,"SELECT * FROM `order` WHERE `o_id`='$id'");
						$row=mysqli_fetch_array($sql);

					}
				?>
				<form method="post" action="order_food_val.php?id= <?php echo $row['o_id']; ?>" enctype="multipart/form-data">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Item Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Item.....!</p>
						</div>';
					}
					
				?>
					
					<div class="form-group">
								<label>Order Number</label>
								<?php
									include("admin/connection.php");
									$date = date('Y-m-d');
									$sql = mysqli_query($con, "SELECT * FROM `order` WHERE o_date = '$date'") or die(mysqli_error($con));
									$count = mysqli_num_rows($sql);
								?>
								<input type="text" name="number" class="form-control" value="<?php echo $row['o_number']; ?>" readonly/>
							</div>
					
							<div class="form-group">
								<label>Coupon Code (Optional)</label>
								<input type="code" name="code" class="form-control" placeholder="Enter Your Coupon Code" value="<?php echo $row['c_code']; ?>" optional/>
							</div>

							<div class="form-group">
								<label>Order Date</label>
								<input type="date" name="date"	class="form-control" min="<?php echo date('Y-m-d'); ?>" value="<?php echo $row['o_date']; ?>" data-validation="required" placeholder="Enter your order date"/>				
							</div>
					
							<div class="form-group">
								<label>Customer Name</label>
								<input type="text" name="name" class="form-control" value="<?php echo $row['cust_name']; ?>" data-validation="required" placeholder="Enter Your Name" required />
							</div>
							
						
							<div class="form-group">
								<label>Item Category</label>
								<select name="category" id="category" data-validation="required" class="form-control" value="<?php echo $row['i_category']; ?>" required="">
										<option value="">--SELECT--</option>
										<?php
											include("admin/connection.php");
											$sql = mysqli_query($con, "SELECT * FROM `category` WHERE cat_type = 'menu'") or die(mysqli_error($con));
											while($row = mysqli_fetch_array($sql))
											{
												echo '<option value="'.$row['cat_id'].'">'.$row['cat_name'].'</option>';
											}
										?>
								</select>
							</div>

							<div class="form-group">
								<label>Selct food Item</label>
								<select name="item_name" id="item_name" data-validation="required" value="<?php echo $row['item_name']; ?>" class="form-control" required/>
								</select>
							</div>
		
							<div class="form-group">
								<label>Unit Price</label>
								<input type="text" name="unit_price" class="form-control" value="<?php echo $row['unitprice']; ?>" id="unitprice" readonly />
							</div>

							<div class="form-group">
								<label>Remaining quantity</label>
								<input type="text"  name="qty" class="form-control" value="<?php echo $row['remain_qty']; ?>" id="remaining_item"  readonly />
							</div>

							<div class="form-group">
								<label>Required Quantity</label>
								<input type="text" name="r_qty"  id="quantity" data-validation="required" class="form-control" value="<?php echo $row['req_qty']; ?>" placeholder="Enter the required quantity" required />
							</div>

							<div class="form-group">
								<label>Total Price</label>
							</div>
					
							<div class="form-group">
								<input type="text" id="total" readonly=""  name="total" value="<?php echo $row['total']; ?>" class="form-control" required="" />
							</div>
						</div>
						<div class="form-group">
							<input type="submit" name="update_btn" class="btn btn-primary btn-block" value="UPDATE">
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>

<?php include("inc/footer.php"); ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#category').on('change',function(){
        var cat_id = $(this).val();
        //alert(cat_id);
        if(cat_id){
            $.ajax({
                type:'POST',
                url:'fetch_food.php',
                data:'cat_id='+cat_id,
                success:function(html){
                   // alert(html);
                    $('#item_name').html(html);
                },
                 error: function (error) {
                  
                }
            }); 
        }else{
            $('#item_name').html('<option value="">Select Category First</option>');
        }
    });


     $('#item_name').on('change',function(){
        var item_name = $(this).val();
        //alert(cat_id);
        if(item_name){
            $.ajax({
                type:'POST',
                url:'fetch_food_price.php',
                data:'item_name='+item_name,
                success:function(html){
                    //alert(html);
                    //$('#item_name').html(html);
                    $('#unitprice').val(html.unit_price);
                    $('#remaining_item').val(html.items_prepared);
                },
                 error: function (error) {
                  
                }
            }); 
        }else{
           // $('#item_name').html('<option value="">Select Category First</option>');
        }
    });
 });
</script>


<script>
$(function(){
    $("#quantity").on("change paste", function() {
        var req_qty = $(this).val();
        var unitprice = $("#unitprice").val();
        var remaining_item = $("#remaining_item").val();

        if(req_qty > remaining_item)
        {
        	$("#total").val("");
        	$("#quantity").val("");
        	alert("Choose Required Quantity Less than "+remaining_item);
        }
        else
        {
        	 var t = unitprice * req_qty;
        	//alert(t);
        	$("#total").val(t);
        }
       
   
        })
});
</script>
</body>
</html>
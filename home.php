<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Fresh Food a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- Default-JavaScript-File -->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
<!-- //Default-JavaScript-File -->

<link rel="stylesheet" href="css/easy-responsive-tabs.css"><!-- easy responsive tabs css -->

<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" media="all"><!-- date-picker css-->

<!-- gallery -->
    <link href="css/lsb.css" rel="stylesheet" type="text/css">
<!-- //gallery -->

    <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style3.css" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
<!-- banner text effect css files -->

<!-- logo text effect css files -->
    <link rel="stylesheet" type="text/css" href="css/demo1.css" />
    <link rel="stylesheet" type="text/css" href="css/linkstyles.css" />
<!-- //logo text effect css files -->
    
<!-- default css files -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/font-awesome.min.css" />
<!-- default css files -->
    
<!--web font-->
    <link href="//fonts.googleapis.com/css?family=Pacifico&amp;subset=latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;subset=cyrillic,latin-ext" rel="stylesheet">
<!--//web font-->

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script> 



</head>

<body>
    <div class="banner jarallax">
        <div class="agileinfo-dot">
            <div class="header">
                <div class="container-fluid">
                    <div class="header-left">
                        <div class="w3layouts-logo grid__item">
                            <h1>
                                <a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
                            </h1>
                        </div>
                    </div>
                    <div class="top-nav">
                        <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                
                                    <?php
                                        if(isset($_SESSION['email']))
                                        {
                                            echo '
                                            <li><a class="active" href="home.php">Home</a></li>
                                                    <li><a href="#about" class="scroll">About</a></li>
                                                    <li><a href="gallary.php">Gallery</a></li>
                                                    <li><a href="#services" class="scroll">Services</a></li>
                                                    <li><a href="food_items.php">Menu Items</a></li>
                                                    <li><a href="orderform.php">Order</a></li>
                                                    <li><a href="feedback.php">Feedback</a></li>     
                                                    <li><a href="#customer" class="scroll">Customers</a></li>
                                        

                                            <li class="dropdown">
                                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">Welcome,'.$user_name.'<span class="caret"></span></a>
                                                    <ul class="dropdown-menu" style="background-color:#333;">
                                                    <li><a href="myorder.php">&nbsp;&nbsp;&nbsp;My Orders</a></li>
                                                        <li><a href="user_profile.php">View Profile</a></li>
                                                        <li><a href="user_password.php">Change Password</a></li>
                                                        <li><a href="index.php">Logout</a></li>
                                                    </ul>
                                                </li>';
                                        }
                                        else
                                        {
                                            echo '
                                            <li><a class="active" href="index.php">Home</a></li>
                                                    <li><a href="#about" class="scroll">About</a></li>
                                                    <li><a href="#services" class="scroll">Services</a></li>
                                                    <li><a href="#chefs" class="scroll">Chefs</a></li>
                                                    <li><a href="#gallery" class="scroll">Gallery</a></li>  
                                                    <li><a href="#customer" class="scroll">Customers</a></li>
                                                    <li><a href="#contact" class="scroll">Contact</a></li>
                                                    <li><a href="user_login.php">Login</a></li>
                                                    <li><a href="user_reg.php">Register</a></li>';
                                        }
                                    ?>
                                    
                                </ul>
                        
                                <div class="clearfix"> </div>                       
                            </div>  
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        </nav>      
                </div>      
            </div>
        </div>
        
        <div class="container"> 
        <div class="row"><br><br><br>           
            <div class="col-md-6 col-md-offset-0">
            <div class="agileinfo-banner-info">
            <h2 style="color:white">Welcome to BOON RELISH FOODS </h2>
                <h2 class="rw-sentence">    
                    <span>Food tastes better, eat with </span>
                    <div class="rw-words rw-words-1">
                        <span>Family</span>
                        <span>Friends</span>
                        <span>happiness</span>
                        <span>Love</span>
                        <span>Gratefulness</span>
                        <span>happiness</span>
                    </div>
                    </h2>
                </div>
            </div>
        
    
        </div>
            </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        </div>
    </div>

    <div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header"> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                        <h4 class="modal-title">Fresh Food</h4>
                        <h5>1 april 2017</h5>
                    </div> 
                    <div class="modal-body">
                    <div class="agileits-w3layouts-info">
                        <img src="images/s1.jpg" alt="" />
                        <p>Morbi eget mollis erat, sit amet feugiat nulla. In hac habitasse platea dictumst. Sed ac fermentum eros. Pellentesque tincidunt nisi sit amet dui lobortis, pulvinar pellentesque dui tempor. Sed iaculis, nisl a eleifend porttitor, diam mauris gravida arcu, suscipit ullamcorper nulla diam vitae lorem..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="demo" id="about">    
    <div class="container"> 
        <div class="w3ls-heading">
            <h3>About us</h3>
        </div>
        <div class="horizontalTab" id="horizontalTab">
            <ul class="resp-tabs-list list-group">
                <li class="list-group-item text-center"></li>
                <li class="list-group-item text-center"></li>
                <li class="list-group-item text-center"></li>
                <li class="list-group-item text-center"></li>
            </ul>
                <div class="resp-tabs-container">
                    <!-- section -->
                            <div class="bhoechie-tab-content active">
                                        <h3 class="title">about</h3>
                                <div class="services-grids">
                                    <div class="ser-img">
                                        <h3>ONLY FRESH AND HEALTHY FOOD FOR YOU AND YOUR FAMILY </h3>
                                        <p>Boon Relish was started in 1994,which was started by just household drink named Boon Soft Drink with lime and Ginger.After some year passed we started preparing Cutlets and Samosa.Now we have a food stall opened in mannagudda with different food items like sandwiches,mini-pizza,etc.Desi items like veg bonda,biscuit rotti,ambade,etc.and we also take huge orders of any veg items.
                                        </p>
                                    </div>
                                    <div class="ser-img1">
                                        <img src="images/about.png" alt="" />
                                    </div>
                                    <div class="ser-info">
                                        <p>Our food items be like noodle-Samosa,cutlet,babycorn-chilly,baked puffs,veg roll,mini-pizza,etc.Daily food items like dosa,idli,rava-idli,sambar,veg bonda,biscuit rotti,ambade,golibaje,podi,rice bath,paner cutlet, kesari-bath,etc.
                                        </p>
                                        <p>Our food items are measured and served to the customer like sambar in liters,item in pieces,so that no one will waste the food.  
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!--  section -->
                            <div class="bhoechie-tab-content">
                                        <h3 class="title ab">Our Menu</h3>
                                <div class="services-grids">
                                    <div class="col-md-6 menugrid">
                                            <img src="images/sandwich2.jpg" alt="" />
                                    </div>
                                    <div class="col-md-6 menugrid1 innergrid">
                                            <h3>Breakfast</h3>
                                            <ul class="list ins1">
                                                <li>Idli Vada - <p>served with Sambar and Chatni</p><span>Rs 30.00</span></li>
                                                <li>Dosa - <p>Served with Sambar and Chatni</p><span>Rs 30.00</span></li>
                                                <li>Rice Bath- <p>served with Kichadi</p><span>Rs 35.00</span></li>
                                                <li>Pongal - <p>served with Chatni</p><span>Rs 20.00</span></li>
                                                <li>Bisibele Bath - <p>served with Kichdi and mixture</p><span>Rs 40.00</span></li>
                                            </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="col-md-6 innergrid">
                                            <h3>Lunch</h3>
                                            <ul class="list ins1">
                                                <li>North Indian Lunch - <p>With Chapathi</p><span>Rs 40.00</span></li>
                                                <li>North Indian Lunch - <p>With Roti</p><span>Rs 40.00</span></li>
                                                <li>South Indian Lunch - <p>With Ragi Mudde</p><span>Rs 50.00</span></li>
                                                <li>South Indian Lunch - <p>With Puri</p><span>Rs 50.00</span></li>
                                                <li>Fruit Salad - <p>served with Ice cream and mix fruits</p><span>Rs 40.00</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 innergrid">
                                                <h3>Snacks</h3>
                                            <ul class="list ins1">
                                                <li>Samosa (Normal and Noodles) - <p>Served with Sause</p><span>Rs 13.00</span></li>
                                                <li>Cutlet - <p>served with Sause</p><span>Rs 15.00</span></li>
                                                <li>Batata Bonda - <p>served with Chatni and Sause</p><span>Rs 15.00</span></li>
                                                <li>Sandwiches - <p>served with Miaoneese</p><span>Rs 20.00</span></li>
                                                <li>BabyCorn - <p>served with Butter,Pepper and other Flavours</p><span>Rs 40.00</span></li>
                                            
                                            </ul>
                                        </div>
                                <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                
                            <!--  search -->
                            <div class="bhoechie-tab-content">
                                        <h3 class="title ab1">Why Visit Us?</h3>
                                <div class="services-grids">
                                    <div class="about-2">
                                        <h3>We select fresh and natural products</h3>
                                        <img src="images/veg-roll2.jpg" alt="" />
                                    </div>
                                    <div class="about-info2">
                                        <div class="top-grid">
                                            <div class="col-md-6 grid">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <h3>Healthy food</h3>
                                                <p>Desi items like veg bonda,biscuit rotti,ambade,etc.and we also take huge orders of any veg items.
                                                </p>
                                            </div>
                                            <div class="col-md-6 grid">
                                                <i class="fa fa-apple" aria-hidden="true"></i>
                                                <h3>Fresh Food</h3>
                                                <p>Desi items like veg bonda,biscuit rotti,ambade,etc.and we also take huge orders of any veg items. 
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="bottom-grid">
                                            <div class="col-md-6 grid">
                                                <i class="fa fa-glass" aria-hidden="true"></i>
                                                <h3>Special Food</h3>
                                                <p>Desi items like veg bonda,biscuit rotti,ambade,etc.and we also take huge orders of any veg items. 
                                                </p>
                                            </div>
                                            <div class="col-md-6 grid">
                                                <i class="fa fa-spoon" aria-hidden="true"></i>
                                                <h3>Tasty snacks</h3>
                                                <p>Desi items like veg bonda,biscuit rotti,ambade,etc.and we also take huge orders of any veg items.
                                                </p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bhoechie-tab-content">
                                        <h3 class="title ab2">why choose us</h3>
                                <div class="services-grids">
                                    <div class="about-2 lastgrid">
                                        <h3>delicious dishes</h3>
                                        <h3>great place to enjoy</h3>
                                        <p>Our food items be like noodle-Samosa,cutlet,babycorn-chilly,baked puffs,veg roll,mini-pizza,etc.Daily food items like dosa,idli,rava-idli,sambar,veg bonda,biscuit rotti,ambade,golibaje,podi,rice bath,paner cutlet, kesari-bath,etc.
                                        </p>
                                    </div>
                                    <div class="about-info2">
                                        <div class="col-md-6 last-grid">
                                            <img src="images/mushroom-butter-masala4.jpg" alt="" />
                                        </div>
                                        <div class="col-md-6 last-grid1">
                                            <h3>Quality And Healthy Food</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                </div>
        </div>
    </div>
</div>


<script src="js/easy-responsive-tabs.js"></script>
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script>

    
    <div class="services" id="services">
        <div class="container">
            <div class="ser-top wthree-3 wow fadeInDown w3-service-head">
                <h3>Our Services </h3>
            </div>
            <div class="w3-service-grids set-6">
                <div class="col-md-3 services-w3-grid1 ser-left icon1 hi-icon-wrap hi-icon-effect-6">
                    <i class="fa fa-cutlery hi-icon hi-icon-sort-by-attributes" aria-hidden="true"></i>
                    <h4>Breakfast</h4>
                    <p>Boon Relish will serve you the better food to eat. Both quality and quantity are similar. Breakfast will be Good, Better and Best. </p>
                </div>
                <div class="col-md-3  services-w3-grid1 ser-left icon2 hi-icon-wrap hi-icon-effect-6">
                    <i class="fa fa-spoon hi-icon hi-icon-sort-by-attributes" aria-hidden="true"></i>
                    <h4>Lunch</h4>
                    <p>Boon Relish will serve you the better food to eat. Both quality and quantity are similar. Lunch items are served nicely and moreover the quality is best to Eat.</p>
                </div>
                <div class="col-md-3 services-w3-grid1 ser-left icon3 hi-icon-wrap hi-icon-effect-6">
                    <i class="fa fa-glass hi-icon hi-icon-sort-by-attributes" aria-hidden="true"></i>
                    <h4>Snacks</h4>
                    <p>Boon Relish will serve you the better food to eat. Both quality and quantity are similar.Some of the snacks are ready to eat with a very good taste.</p>
                </div>
                <!--<div class="clearfix"></div>-->
                <div class="col-md-3 services-w3-grid1 ser-left icon3 hi-icon-wrap hi-icon-effect-6">
                    <i class="fa fa-glass hi-icon hi-icon-sort-by-attributes" aria-hidden="true"></i>
                    <h4>Special Foods</h4>
                    <p>Boon Relish will serve you the better food to eat. Both quality and quantity are similar.Special foods are some of the foods which are made with a special interest. </p>
                </div>
                <div class="clearfix"></div>
            </div>  
        </div>
</div>
<!-- /services -->

<!-- our chefs -->
<!-- //our chefs -->

    
<!-- gallery -->
<!-- //gallery -->
<!-- gallery js -->
    <script src="js/lsb.min.js"></script>
    <script>
    $(window).load(function() {
          $.fn.lightspeedBox();
        });
    </script>
<!-- //gallery js -->

<!-- customer -->
    <div class="customer jarallax" id="customer">
        <div class="agileinfo-dot">
        <div class="container">
            <h3>Customer Feedback</h3>
            <?php
                include("admin/connection.php");
                $sql = mysqli_query($con, "SELECT * FROM `feedback` ORDER BY f_id DESC LIMIT 5") or die(mysqli_error($con));
                while($row = mysqli_fetch_array($sql))
                {
                    echo '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h5>'.$row['f_name'].'</h5>
                            </div>

                            <div class="panel-body">
                                <p>'.nl2br($row['f_message']).'</p>
                            </div>

                            <div class="panel-footer">
                                <p>'.$row['f_num'].'</p>
                            </div>
                        </div>';
                }
            ?>

        </div>
    </div>
</div>
<!-- //customer -->
    
<!-- contact form -->
<!-- //contact form -->

<!-- map -->
<div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56801282.36779669!2d-131.48418691999598!3d29.66539724812115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6dc13a009bba0319!2sPret+A+Manger!5e0!3m2!1sen!2sin!4v1491030489371"></iframe>
        <div class="agile_map_grid">
            <div class="agile_map_grid1">
                <h3>Contact US</h3>
                <ul>
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>address :</span> Boon, Foods and Berevages, Gundu Rao Lane, Mannnagudda, Mangalore - 575 003.</li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><span>email :</span> <a href="mailto:boonfoods@gmail.com">boonfoods@gmail.com</a></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i><span>call us :</span> Ph: 0824-2456 968,   9448126968 </li>
                </ul>
            </div>
        </div>
    </div>
<!-- //map -->
<!-- footer -->
<div class="footer">
        <div class="col-md-12 footer-left">
            <h3>Boon Relish</h3>
            <p>Boon relish is one of the good food service provider. The foods are good in quality and therer are many customers rush towords them to have a good quality and quantity food. </p>
            <p>Boon relish contains some of the food item categories like Snacks, Lunch and some of the Special food items. </p>
        </div>
        <div class="clearfix"></div>
</div>
<!-- //footer -->

<!-- copyright -->
<!-- //copyright -->

<script src="js/jarallax.js"></script>
    <script src="js/SmoothScroll.min.js"></script>
    <script type="text/javascript">
        /* init Jarallax */
        $('.jarallax').jarallax({
            speed: 0.5,
            imgWidth: 1366,
            imgHeight: 768
        })
    </script>
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/easing.js"></script>
    <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
            */
                                
            $().UItoTop({ easingType: 'easeOutQuart' });
                                
            });
    </script>
<!-- //here ends scrolling icon -->


<!-- Date-Picker-JavaScript -->
                <script src="js/jquery-ui.js"></script>
                <script>
                    $(function() {
                        $( "#datepicker,#datepicker1,#datepicker2" ).datepicker();
                    });
                </script>
<!-- //Date-Picker-JavaScript -->

<!-- banner text effect js file -->
        <script src="js/modernizr.custom.72111.js"></script>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

    <div class="container">
    <div class="row"><br>
        <?php 
            $_SESSION['order_no'] =  "BR-".date('Ymd').rand(0,10);
        ?>
        </div>
    </div>


</body>

</html>

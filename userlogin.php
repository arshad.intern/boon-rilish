<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="well login_form">
				<h4>LOGIN</h4>
				<hr/>
				<form method="post" action="">
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="name" class="form-control" placeholder="Enter Your Name" required/>
					</div>
			
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Enter Your Password" required>
					</div>
					
					<div class="form-group">
						<input type="submit" name="log_btn" class="btn btn-primary btn-block" value="Login">
					</div>
				</form>
				<p>Didn't Register Yet? <a href="user_reg.php">Register Now </a></p>
			</div>
		</div>
	</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

<?php
//Include database configuration file
include('admin/connection.php');

if(isset($_POST["item_name"])){

	$item_name = $_POST['item_name'];
	$query = mysqli_query($con, "SELECT * FROM items WHERE i_name = '$item_name'") or die(mysqli_error($con));
    $rowCount =mysqli_num_rows($query);
    
     if($rowCount > 0){
        $row = mysqli_fetch_array($query);
        header("Content-type: application/json");
        echo  json_encode(array("unit_price" => $row['i_unitprice'], "items_prepared" => $row['i_prepared']));
    }else{
        echo 'No Matching Items found';
    }
}

?>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW ITEM<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<?php
					if(isset($_GET['update']))
					{

						include("connection.php");
						$id=$_GET['id'];
						$sql=mysqli_query($con,"SELECT * FROM `items` WHERE `i_id`='$id'");
						$row=mysqli_fetch_array($sql);

					}
				?>
				<form method="post" action="item_val.php?id=<?php echo $row['i_id']; ?>" enctype="multipart/form-data">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Item Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Item.....!</p>
						</div>';
					}
					
				?>
					
					<div class="form-group">
						<label>Item Category </label>
						<input type="text" name="category"  data-validation="required" class="form-control" value="<?php echo $row['i_category']; ?>" placeholder="Enter Item Category" required/>
					</div>
			
					<div class="form-group">
						<label>Item Name</label>
						<input type="text" id="name" name="name"  data-validation="required" class="form-control" value="<?php echo $row['i_name']; ?>" placeholder="Enter Item name" required>
					</div>
			
					<!--<div class="form-group">
						<label>Item Image</label>
					</div>-->
			
					<div class="form-group">
						<label>Item Sub Title</label>
						<input type="text" name="subtitle" data-validation="required" class="form-control" value="<?php echo $row['i_subtitle']; ?>" placeholder="Enter Item subtitle" required>
					</div>

					<div class="form-group">
						<label>Measuring Term</label>
						<input type="text" name="measure"  data-validation="required" class="form-control" value="<?php echo $row['i_measure']; ?>" placeholder="Enter Item Measurment" required/>
					</div>

					<div class="form-group">
						<label>Item Unit Price</label>
						<input type="text" name="price" data-validation="required" class="form-control" value="<?php echo $row['i_unitprice']; ?>" placeholder="Enter Unit Price" required>
					</div>
					
					<div class="form-group">
						<label>Items Prepared</label>
						<input type="text" name="items" data-validation="required"  class="form-control" value="<?php echo $row['i_prepared']; ?>" placeholder="Enter items prepared" required>
					</div>

					<div class="form-group">
						<input type="submit" name="update_btn" class="btn btn-primary btn-block" value="UPDATE">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>
		
</body>
</html>
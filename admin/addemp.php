<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push" style="background:url(mayyia4.png);background-repeat: no-repeat; background-size:100% 100%">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW EMPLOYEE</h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Employee Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Employee.....!</p>
						</div>';
					}
				?>

				<form method="post" action="emp_val.php" enctype="multipart/form-data">
					<div class="form-group">
						<label for="username">Full Name </label>
						<input type="text" id="name" name="name" class="form-control"  data-validation="required" placeholder="Enter Your Full Name" required/>
					</div>
			
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" class="form-control" id="extra7" max="10" data-validation="required number length" data-validation-length="min10" name="phone" placeholder="Enter Your Phone Number" onkeypress="return isNumber(event)" required="" />
					</div>
			
					<div class="form-group">
						<label>Gender</label>

						<?php/*
							$male_checked = $female_checked = "";
							$check = $rrc['gender'];
							if($check == "Male")
							{
								$male_checked = "checked";
							}
							else if($check == "Female"){
								$female_checked = "checked";
							}*/
						?>

						<input type="radio" name="gender" data-validation="required" value="Male" required/> Male
						<input type="radio" name="gender" data-validation="required" value="Female" required/> Female
					</div>
			
					<div class="form-group">
						<label>Address</label>(<span id="maxlength">100</span> characters left)
						<textarea rows="4" name="address" id="area" class="form-control" data-validation="required" placeholder="Enter Your Address" required>
						</textarea>
					</div>
			
					<div class="form-group">
						<label>Email-ID</label>
						<input type="email" name="email_id" class="form-control" data-validation="required email" placeholder="Enter Your Email-ID" required>
					</div>

					<div class="form-group">
						<label>Date of Join</label>
						<input type="date" name="doj" class="form-control" data-validation="required date" placeholder="Enter Your Date of Join" required>
					</div>

					<div class="form-group">
						<label>Year of Experience</label>
						<input type="text" name="experience" id="extra7" class="form-control" data-validation="required number length" data-validation-length="max2" placeholder="Enter Your Experience in years" onkeypress="return isNumber(event)" required>
					</div>

					<div class="form-group">
						<label>Adhar ID</label>
						<input type="text" name="adhar" id="uid" class="form-control" data-validation="required number length"  data-validation-length="min12" onkeypress="return isNumber(event)" placeholder="Enter Your Adhar ID">
					</div>

					<div class="form-group">
						<label>Choose Photo</label>
						<input type="file" name="photo" id="file"  />
						<br />
					</div>
					
					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="ADD">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		

		
		<?php include("inc/footer.php"); ?>

		<script>
  			$('#area').restrictLength($('#maxlength'));
		</script>

		 <script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>
		
		<script>
			function isNumber(evt) 
			{
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57))
			    {
			        return false;
    			}
   				 return true;
			}

		</script>
		
</body>
</html>


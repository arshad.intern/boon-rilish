<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>BOON RELISH FOOD SERVICE SYSTEM </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts--> 
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
</head> 
<body>
	<div class="main-content">
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--logo -->
				<div class="logo">
					<a href="index.php">
						<h1>Boon Relish</h1>
						<span>AdminPanel</span>
					</a>
				</div>
				<!--//logo-->
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page ">
				<div class="widget-shadow">
					<div class="login-top">
						<h4>Welcome back to Boon Relish AdminPanel ! <br> SIGN IN</a> </h4>
					</div>
					<div class="login-body">
						<form method="post" action="">
							<select name="user_role" class="form-control" style="border-radius: 0px;">
								<option value="1">Admin</option>
								<option value="2">Employee</option>
							</select><br>
							<input type="text" class="user" name="email" data-validation="required email" placeholder="Enter your email" required="">
							<input type="password" name="password"  data-validation="required" class="lock" placeholder="password">
							
							<input type="submit" name="login_btns" value="Sign In">
							<div class="forgot-grid">
								<div class="forgot">
									<a href="forgotpassword.php">forgot password?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
						</form>
						<?php
							include("connection.php");
							if(isset($_POST['login_btns']))
							{
								$email_id = $_POST['email'];
								$password = $_POST['password'];
								$user_role = $_POST['user_role'];
                                $query = mysqli_query($con, "SELECT * FROM `login` WHERE `email` = '$email_id' AND `password` = '$password' AND user_role = '$user_role'") or die(mysqli_error($con));
									$row = mysqli_fetch_array($query);

								if($row)
								{
									session_start();
									$_SESSION['a_email'] = $row['email'];
									$_SESSION['a_name'] = $row['name'];
									$_SESSION['user_role'] = $row['user_role'];
									if($row['status'] == "0")
									{
										echo '<h5 style="color:red; text-align:center;"> Admin Blocked You...!!! Please try login in after sometimes, Or Contact admin if problem exists.</h5>';
									}
									else
									{
										header("location:home.php?logged_in");
									}
									
								}
								else
								{
									echo '<h4 style="color:red; text-align:center;"> Invalid Email ID and Password..</h4>';
								}
							}

						?>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; <?php echo date('Y'); ?> Boon Relish. All Rights Reserved | Design by Subramanya, Manamohan, Ravichandra</a></p>
		</div>
        <!--//footer-->
	</div>
	<!-- Classie -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<script>
  		$.validate({
    	lang: 'en'
  		});
	</script>
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>

</body>
</html>
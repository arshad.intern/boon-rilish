<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Blank Page</h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<h4>ADD NEW EMPLOYEE</h4>
				<hr/>
				<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Employee Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Employee.....!</p>
						</div>';
					}
				?>

				<form method="post" action="emp_val.php">
					<div class="form-group">
						<label>Full Name </label>
						<input type="text" name="fname" class="form-control" placeholder="Enter Your Full Name" required/>
					</div>
			
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" name="phone" class="form-control" placeholder="Enter Your Phone Number" required>
					</div>
			
					<div class="form-group">
						<label>Gender</label>

						<?php/*
							$male_checked = $female_checked = "";
							$check = $rrc['gender'];
							if($check == "Male")
							{
								$male_checked = "checked";
							}
							else if($check == "Female"){
								$female_checked = "checked";
							}*/
						?>

						<input type="radio" name="gender" value="Male" required/> Male
						<input type="radio" name="gender" value="Female" required/> Female
					</div>
			
					<div class="form-group">
						<label>Address</label>
						<textarea rows="4" name="address"class="form-control" placeholder="Enter Your Address" required></textarea>
					</div>
			
					<div class="form-group">
						<label>Email-ID</label>
						<input type="email" name="email-id" class="form-control" placeholder="Enter Your Email-ID" required>
					</div>

					<div class="form-group">
						<label>Date of Join</label>
						<input type="text" name="doj" class="form-control" placeholder="Enter Your Date of Join" required>
					</div>

					<div class="form-group">
						<label>Year of Experience</label>
						<input type="text" name="experience" class="form-control" placeholder="Enter Your Experience in years" required>
					</div>

					<div class="form-group">
						<label>Adhar ID</label>
						<input type="text" name="adhar" class="form-control" placeholder="Enter Your Adhar ID">
					</div>
					
					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="ADD">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
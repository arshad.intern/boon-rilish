<div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
					<ul class="nav" id="side-menu">
						<li>
							<a href="home.php"><i class="fa fa-home nav_icon"></i>Dashboard</a>
						</li>

						<?php

							$user_role=$_SESSION['user_role'];

							if($user_role == 1)
							{
						?>

						<li class="">
							<a href="#"><i class="fa fa-cogs nav_icon"></i>Menu Category <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="add_category.php">Add New Category</a>
								</li>
								<li>
									<a href="manage_category.php">Manage Category</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>

						<li class="">
							<a href="#"><i class="fa fa-book nav_icon"></i>Menu Items <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="menuitems.php">Add New Menu</a>
								</li>
								<li>
									<a href="manageitems.php">Manage Menu Items</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>

						<li>
							<a href="#"><i class="fa fa-cogs nav_icon"></i>Manage Recepie <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="add_reciepy.php">Add New Recepie</a>
								</li>
								<li>
									<a href="manage_reciepy.php">Manage Recepie</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>

						<li>
							<a href="#"><i class="fa fa-cogs nav_icon"></i>Employees <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="addemp.php">Add New Employee</a>
								</li>
								<li>
									<a href="manage_employee.php">Manage Employee</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>

						<li>
							<a href="manageusers.php"><i class="fa fa-users nav_icon"></i>Manage Users</a>
						</li>

						<li>
							<a href="manageorder.php"><i class="fa fa-th-large nav_icon"></i>Manage Orders</a>
						</li>

						
						
						<li>
							<a href="#"><i class="fa fa-envelope nav_icon"></i>Stocks<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="addstock.php">Add New Stock</a>
								</li>
								<li>
									<a href="managestocks.php">Manage Stocks</a>
								</li>
							</ul>
							<!-- //nav-second-level -->
						</li>
						<li>
							<a href="recepie_calculator.php"><i class="fa fa-table nav_icon"></i>Recepie Calculator</a>
						</li>
					
						<li>
							<a href="#"><i class="fa fa-file-text-o nav_icon"></i>Coupens<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="addcoupen.php">Add New Coupen</a>
								</li>
								<li>
									<a href="manage_coupen.php">Manage Coupens</a>
								</li>
							</ul>
							<!-- //nav-second-level -->
						</li>
						<!--<li>
							<a href="charts.html" class="chart-nav"><i class="fa fa-bar-chart nav_icon"></i>Billing Info. </a>
						</li>-->

						<li>
							<a href="feedback_details.php" class="chart-nav"><i class="fa fa-users nav_icon"></i>Feedbacks</a>
						</li>
						<?php
					}
					else if($user_role ==2)
					{

						?>

							<li>
							<a href="manageorder.php"><i class="fa fa-th-large nav_icon"></i>Manage Orders</a>
						</li>

						<li class="">
							<a href="#"><i class="fa fa-cogs nav_icon"></i>Category <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="add_category.php">Add New Category</a>
								</li>
								<li>
									<a href="manage_category.php">Manage Category</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>

						<li class="">
							<a href="#"><i class="fa fa-book nav_icon"></i>Menu Items <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="menuitems.php">Add New Menu</a>
								</li>
								<li>
									<a href="manageitems.php">Manage Menu Items</a>
								</li>
							</ul>
							<!-- /nav-second-level -->
						</li>
						
						

						<li>
							<a href="#"><i class="fa fa-envelope nav_icon"></i>Stocks<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="addstock.php">Add New Stock</a>
								</li>
								<li>
									<a href="managestocks.php">Manage Stocks</a>
								</li>
							</ul>
							<!-- //nav-second-level -->
						</li>

						<!--<li>
							<a href="#"><i class="fa fa-file-text-o nav_icon"></i>Manage Receipt</a>
							 //nav-second-level 
						</li>-->

						<li>
							<a href="#"><i class="fa fa-file-text-o nav_icon"></i>Coupens<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li>
									<a href="addcoupen.php">Add New Coupen</a>
								</li>
								<li>
									<a href="manage_coupen.php">Manage Coupens</a>
								</li>
							</ul>
							<!-- //nav-second-level -->
					
						</li>
					<?php
					}

						?>

					</ul>
					<div class="clearfix"> </div>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
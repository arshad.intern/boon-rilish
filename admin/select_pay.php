<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
		<form name="" method="post" action="" id="myform">
									
										<input type="hidden" name="order_no" value="<?php echo $order_no; ?>" />
										<ul class="donate-now">
											<li>	
												<input type="radio" id="payment_method" name="payment_method" value="a" >
												<label for="payment_method"><i class="fa fa-globe fa-5x"></i><br>Online Payment</label>
											</li>
											
											<li>
												<input type="radio" id="payment_method1" name="payment_method1" value="b" >
												<label for="payment_method1"><i class="fa fa-dollar fa-5x"></i><br>Cash on Delivery</label>
											</li>
										</ul>
										
										
										<input type="hidden" name="qty" value="<?php echo $quppp ;?>">
										<input type="hidden" name="gtot" value="<?php echo $tot_price ;?>">
									
									</div>
									</form>
									<script>
											$('input[type=radio]').change(function(){
											$('#myform').submit();
											});
									</script>
	</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

</body>

</html>

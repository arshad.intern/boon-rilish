<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW ITEM <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<form method="post" action="item_val.php" enctype="multipart/form-data">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Item Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Item.....!</p>
						</div>';
					}
					else if(isset($_GET['already_exists']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Sub Item Already Exists.....!</p>
						</div>';
					}
				?>
					
					<div class="form-group">
						<label>Item Category </label>
						<select class="form-control" data-validation="required" name="category">
							<option value="">--SELECT--</option>
							<?php
								include("connection.php"); 
								$sql = mysqli_query($con, "SELECT * FROM `category` WHERE cat_type = 'menu'") or die(mysqli_error($con));
								while($row = mysqli_fetch_array($sql))
								{
									echo '<option value="'.$row['cat_id'].'">'.$row['cat_name'].'</option>';
								}
							?>
						</select>
					</div>
			
					<div class="form-group">
						<label>Item Name</label>
						<input type="text" id="name" name="name" data-validation="required" class="form-control" placeholder="Enter Item name" required>
					</div>
			
					<!--<div class="form-group">
						<label>Item Image</label>
					</div>-->
			
					<div class="form-group">
						<label>Item Subtitle</label>
						<input type="text" id="bbb" data-validation="required" name="subtitle" class="form-control" placeholder="Enter Item Subtitle" required>
					</div>

					<div class="form-group">
						<label>Measureing Term</label>
						<select class="form-control" data-validation="required" name="measure">
							<option value="">--SELECT--</option>
							<option>Kgs</option>
							<option>Grams</option>
							<option>Piece</option>
						</select>
					</div>

					<div class="form-group">
						<label>Item Unit Price</label>
						<input type="text" class="form-control" id="extra7" data-validation="required number length" data-validation-length="max10" name="price" placeholder="Enter Your Phone Number" onkeypress="return isNumber(event)" required="" />
					</div>
					
					<div class="form-group">
						<label>Items Prepared</label>
						<input type="text" class="form-control" id="extra7" data-validation="required number length" data-validation-length="max10" name="items" placeholder="Enter Your Phone Number" onkeypress="return isNumber(event)" required="" />
					</div>

					<div class="form-group">
						<label>Choose Photo</label>
						<input type="file" name="i_image" id="file"/>
						<br />
					</div>

					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="ADD">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
            $( document ).ready(function() {
                $( "#bbb" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
			function isNumber(evt) 
			{
			    evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57))
			    {
			        return false;
    			}
   				 return true;
			}

		</script>

		

</body>
</html>
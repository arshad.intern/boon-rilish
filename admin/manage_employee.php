<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Employee <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Employee Name</th>
								<th>Phone</th>
								<th>Email Id</th>
								<th>Block/UnBlock</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `employees` INNER JOIN `login` ON `employees`.e_email = `login`.email") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['e_name'].'</td>
							<td>'.$row['e_phone'].'</td>
							<td>'.$row['e_email'].'</td>
							<td>';


							if($row['status'] == "0")
							{
								echo '<a href="emp_val.php?unblock&employee_id='.$row['e_id'].'" class="btn btn-success btn-sm"><i class="fa fa-user fa-fw"></i>Unblock Employee</a>';
							}
							else
							{
								echo '<a href="emp_val.php?block&employee_id='.$row['e_id'].'" class = "btn btn-danger btn-sm"><i class="fa fa-user fa-fw"></i>Block Employee </a>';
							}


							echo '</td>
							<td>
								<div class="btn-group">
								<a href="emp_details.php?emp_id='.$row['e_id'].'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
									<a href="editemp.php?update&id='.$row['e_id'].'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
		

			$sql = mysqli_query($con, "SELECT * FROM `feedback`") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);
		?>
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Feedbacks <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Name</th>
								<th>Email ID</th>
								<th>Phone</th>
								<th>Message</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

					<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `feedback`") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['f_name'].'</td>
							<td>'.$row['f_email'].'</td>
							<td>'.$row['f_num'].'</td>
							<td>'.$row['f_message'].'</td>
							<td>
								<div class="btn-group">
								<a href="feed_details.php?feed_id='.$row['f_id'].'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
								<a href="feed_val.php?delete&feed_id='.$row['f_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
							</tr>';
						}
					?>

						</tbody>	
					</table>

				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
			
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Users:
								<?php
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `users`") or die(mysqli_error($con));
									$count_users = mysqli_num_rows($sql);
									echo $count_users;
								?>
							</h4>
						</div>
					</div>

					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Employees:
								<?php 
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `employees`") or die(mysqli_error($con));
									$count_employees = mysqli_num_rows($sql);
									echo $count_employees;
								?>
							</h4>
						</div>
					</div>

					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Orders: 
								<?php 
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `order`") or die(mysqli_error($con));
									$count_order = mysqli_num_rows($sql);
									echo $count_order;
								?>
							</h4>
						</div>
					</div>

					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Stocks:
								<?php 
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `stocks`") or die(mysqli_error($con));
									$count_stocks = mysqli_num_rows($sql);
									echo $count_stocks;
								?>
							</h4>
						</div>
					</div>

					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Food Items:
								<?php 
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `items`") or die(mysqli_error($con));
									$count_items = mysqli_num_rows($sql);
									echo $count_items;
								?>
							</h4>
						</div>
					</div>

					<div class="col-md-4">
						<div class="well">
							<h4 class="text-center">Total Recipies:
								<?php 
									include("connection.php");
									$sql = mysqli_query($con, "SELECT * FROM `reciepy`") or die(mysqli_error($con));
									$count_reciepy = mysqli_num_rows($sql);
									echo $count_reciepy;
								?>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div></div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
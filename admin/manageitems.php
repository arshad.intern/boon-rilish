<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Items<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Item Category</th>
								<th>Item Name</th>
								<th>Item Subtitle</th>
								<th>Image</th>
								<th>Measuring Terms</th>
								<th>Unit Price</th>
								<th>Item Prepared</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

						<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `items` INNER JOIN category ON `items`.i_category = `category`.cat_id") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['cat_name'].'</td>
							<td>'.$row['i_name'].'</td>
							<td>'.$row['i_subtitle'].'</td>
							<td><img src="emp_profile/'.$row['i_image'].'" class="img-responsive" style="width:100px;height:50px;" /></td>
							<td>'.$row['i_measure'].'</td>
							<td>'.$row['i_unitprice'].'</td>
							<td>'.$row['i_prepared'].'</td>
							<td>
								<div class="btn-group">
									<a href="item_details.php?i_id='.$row['i_id'].'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
									<a href="edititem.php?update&id='.$row['i_id'].'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
									<a href="item_val.php?delete&item_id='.$row['i_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
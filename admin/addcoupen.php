<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW COUPEN <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<form method="post" action="coupen_val.php">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Coupen Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Coupen.....!</p>
						</div>';
					}
				?>
			
					<div class="form-group">
						<label>Coupen Title</label>
						<input type="text" id="name" name="title" class="form-control" data-validation="required" placeholder="Enter Coupen Title" required>
					</div>
			
					<!--<div class="form-group">
						<label>Item Image</label>
					</div>-->
			
					<div class="form-group">
						<label>Coupen Code</label>
						<input type="text" name="code" class="form-control" data-validation="required" placeholder="Enter Coupen Code" required>
					</div>

					<div class="form-group">
						<label>Coupen Discount</label>
						<input type="text" name="discount" class="form-control" data-validation="required" placeholder="Enter Coupen Discount" required>
					</div>
					
					<div class="form-group">
						<label>Coupen Created Date</label>
						<input type="date" name="date" data-validation="required date" class="form-control" min="<?php echo date('Y-m-d'); ?>" placeholder="Enter Created Date" required>
					</div>

					<div class="form-group">
						<label>Coupen Validity</label>
						<input type="date" name="validity" data-validation="required" class="form-control" min="<?php echo date('Y-m-d'); ?>" placeholder="Enter Coupen Validity" required>
					</div>

					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="ADD">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

</body>
</html>
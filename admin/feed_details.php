<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
			$f_id = $_GET['feed_id'];

			$sql = mysqli_query($con, "SELECT * FROM `feedback` WHERE f_id = '$f_id'") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);

			/*$emp_profile= $row['e_photo'];
			if($emp_profile == "")
			{
				$emp_profile = "images/user.png";
			}
			else
			{
				$emp_profile = "emp_profile/".$emp_profile;
			}*/
		?>
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Feedbacks</h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<tr>
									<th>User Name</th>
									<td><?php echo $row['f_name']; ?></td>
								</tr>

								<tr>
									<th>Phone Number</th>
									<td><?php echo $row['f_num']; ?></td>
								</tr>

								<tr>
									<th>Email</th>
									<td><?php echo $row['f_email']; ?></td>
								</tr>

								<tr>
									<th>Message</th>
									<td><?php echo $row['f_message']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
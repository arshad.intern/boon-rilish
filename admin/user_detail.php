<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
			$usr_id = $_GET['user_id'];

			$sql = mysqli_query($con, "SELECT * FROM `users` WHERE usr_id = '$user_id'") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);
		?>
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Users</h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-12">
							<table class="table">
								<tr>
									<th>User Name</th>
									<td><?php echo $row['name']; ?></td>
								</tr>

								<tr>
									<th>Email-ID</th>
									<td><?php echo $row['email']; ?></td>
								</tr>

								<tr>
									<th>Gender</th>
									<td><?php echo $row['gender']; ?></td>
								</tr>

								<tr>
									<th>Contact No</th>
									<td><?php echo $row['phone']; ?></td>
								</tr>

								<tr>
									<th>Address</th>
									<td><?php echo $row['e_address']; ?></td>
								</tr>


							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
			$emp_id = $_GET['emp_id'];

			$sql = mysqli_query($con, "SELECT * FROM `employees` WHERE e_id = '$emp_id'") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);

			$emp_profile= $row['e_photo'];
			if($emp_profile == "")
			{
				$emp_profile = "images/user.png";
			}
			else
			{
				$emp_profile = "emp_profile/".$emp_profile;
			}
		?>
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Employee<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-3">
							<div class="thumbnail">
								<img src="<?php echo $emp_profile; ?>" class="img-responsive img-circle">
							</div>
						</div>
						<div class="col-md-9">
							<table class="table">
								<tr>
									<th>Employee Name</th>
									<td><?php echo $row['e_name']; ?></td>
								</tr>

								<tr>
									<th>Phone Number</th>
									<td><?php echo $row['e_phone']; ?></td>
								</tr>

								<tr>
									<th>Gender</th>
									<td><?php echo $row['e_gender']; ?></td>
								</tr>

								<tr>
									<th>Address</th>
									<td><?php echo $row['e_address']; ?></td>
								</tr>

								<tr>
									<th>Email</th>
									<td><?php echo $row['e_email']; ?></td>
								</tr>

								<tr>
									<th>Date of Join</th>
									<td><?php echo $row['e_doj']; ?></td>
								</tr>

								<tr>
									<th>Work Experience</th>
									<td><?php echo $row['e_experience']; ?></td>
								</tr>


								<tr>
									<th>Aadhar Number</th>
									<td><?php echo $row['e_adhar']; ?></td>
								</tr>


							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
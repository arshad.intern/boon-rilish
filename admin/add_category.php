<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push" style="background:url(mayyia4.png); background-repeat: no-repeat; background-size: 100% 100%">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW CATEGORY <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Employee Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Employee.....!</p>
						</div>';
					}
					else if(isset($_GET['already_exists']))
					{
						echo'<div class="alert alert-info">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Oops.....!</b>Category Already Exists.....!</p>
						</div>';
					}
				?>

				<form method="post" action="cat_val.php">
					<div class="form-group">
						<label>Categoy Name: </label>
						<input type="text" id="name" name="cat_name" class="form-control" data-validation="required" placeholder="Enter Your Category Name" required/>
					</div>

					<div class="form-group">
						<label>Select Category Type:</label>
						<select name="cat_type" data-validation="required" class="form-control">
							<option value="">---SELECT---</option>
							<option value="menu">For Menu Items</option>
							<option value="stock">For Stocks</option>
						</select>
					</div>

					<div class="form-group">
						<input type="submit" name="add_category" class="btn btn-primary btn-block" value="ADD NEW CATEGORY">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>
</body>
</html>


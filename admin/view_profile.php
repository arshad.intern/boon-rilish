<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
			//$u_id = $_GET['user_id'];

			$sql = mysqli_query($con, "SELECT `u_id`, `name`, `email`, `gender`, `address`, `phone`, `password` FROM `login` where `email`='$a_email' ") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);

			
		?>

		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Admin Profile<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-12">
							<form name="" method="post" action="update_profile.php?admin_id=<?php echo $row[0]; ?>">
							<table class="table">
								<tr>
									<th>Name</th>
									<td><input type="text" name="name" class="form-control" value="<?php echo $row['name']; ?>" /></td>
								</tr>

								<tr>
									<th>Email Address</th>
									<td><input type="text" name="email" class="form-control" value="<?php echo $row['email']; ?>" /></td>
								</tr>

								<tr>
									<th>Gender</th>
									<td><input type="text" name="gender" class="form-control" value="<?php echo $row['gender']; ?>"></td>
								</tr>

								<tr>
									<th>Address</th>
									<td><input type="text" name="address" class="form-control" value="<?php echo $row['address']; ?>"></td>
								</tr>

								<tr>
									<th>Phone Number</th>
									<td><input type="number" name="phone" class="form-control" value="<?php echo $row['phone']; ?>"></td>
								</tr>
							</table>

							<input type="submit" name="update_profile" value="Update" class="btn btn-primary center-block">
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
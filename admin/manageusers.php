<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Customer <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Name</th>
								<th>Email ID</th>
								<th>Gender</th>
								<th>Phone</th>
								<th>Address</th>
								<th>Block/Unblock</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `users`") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['name'].'</td>
							<td>'.$row['email'].'</td>
							<td>'.$row['gender'].'</td>
							<td>'.$row['phone'].'</td>
							<td>'.$row['address'].'</td>

							<td>';

							if($row['status'] == "0")
							{
								echo '<a href="users_val.php?unblock&user_id='.$row['user_id'].'" class="btn btn-success btn-sm"><i class="fa fa-user fa-fw"></i>Unblock User </a>';
							}
							else
							{
								echo '<a href="users_val.php?block&user_id='.$row['user_id'].'" class = "btn btn-danger btn-sm"><i class="fa fa-user fa-fw"></i>Block User </a>';
							}

							echo '</td>

							<td>
								<div class="btn-group">
									<a href="users_val.php?delete&user_id='.$row['user_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i>Delete</a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
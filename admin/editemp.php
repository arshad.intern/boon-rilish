<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">EDIT EMPLOYEE DETAILS<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Employee Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Employee.....!</p>
						</div>';
					}

					if(isset($_GET['update']))
					{

						include("connection.php");
						$id=$_GET['id'];
						$sql=mysqli_query($con,"SELECT * FROM `employees` WHERE `e_id`='$id'");
						$row=mysqli_fetch_array($sql);

					}
				?>

				<form method="post" action="emp_val.php?id=<?php echo $row['e_id']; ?>" enctype="multipart/form-data">
					<div class="form-group">
						<label>Full Name </label>
						<input type="text" id="name" name="name" data-validation="required" class="form-control" placeholder="Enter Your Full Name" value="<?php echo $row['e_name']; ?>" required/>
					</div>
			
					<div class="form-group">
						<label>Phone Number</label>
						<input type="text" name="phone" data-validation="required number" class="form-control" placeholder="Enter Your Phone Number" value="<?php echo $row['e_phone']; ?>" required>
					</div>
			
					<div class="form-group">
						<label>Gender</label>

						<?php
							$male_checked = $female_checked = "";
							 $check = $row['e_gender'];
							if($check == "Male")
							{
								$male_checked = "checked";
							}
							else if($check == "Female"){
								$female_checked = "checked";
							}
						?>

						<input type="radio" name="gender" data-validation="required" value="Male" required <?php echo $male_checked; ?>/> Male
						<input type="radio" name="gender" data-validation="required" value="Female" required <?php echo $female_checked; ?>/> Female
					</div>
			
					<div class="form-group">
						<label>Address</label>(<span id="maxlength">50</span> characters left)
						<textarea rows="4" name="address" id="area" data-validation="required" class="form-control" placeholder="Enter Your Address"  required><?php echo $row['e_address']; ?></textarea>
					</div>
			
					<div class="form-group">
						<label>Email-ID</label>
						<input type="email" name="email_id" data-validation="required email" class="form-control" placeholder="Enter Your Email-ID" value="<?php echo $row['e_email']; ?>" required>
					</div>

					<div class="form-group">
						<label>Date of Join</label>
						<input type="text" name="doj" data-validation="required date" class="form-control" placeholder="Enter Your Date of Join" value="<?php echo $row['e_doj']; ?>"required>
					</div>

					<div class="form-group">
						<label>Year of Experience</label>
						<input type="text" name="experience" data-validation="required" class="form-control" placeholder="Enter Your Experience in years" value="<?php echo $row['e_experience']; ?>"required>
					</div>

					<div class="form-group">
						<label>Adhar ID</label>
						<input type="text" name="adhar" data-validation="required" class="form-control" value="<?php echo $row['e_adhar']; ?>"placeholder="Enter Your Adhar ID">
					</div>

					<div class="form-group">
						<label>Choose Photo</label>
						<input type="file" name="photo" id="file"  />
						<br />
					</div>
					
					<div class="form-group">
						<input type="submit" name="update_btn" class="btn btn-primary btn-block" value="UPDATE">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
		<script>
  			$('#area').restrictLength($('#maxlength'));
		</script>
		
		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

</body>
</html>


<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Stocks<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Stock Category</th>
								<th>Stock Name</th>
								<th>Stock Quantity</th>
								<th>Received Date</th>
								<th>Vendor Name</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `stocks` INNER JOIN category ON `stocks`.s_category = `category`.cat_id") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['cat_name'].'</td>
							<td>'.$row['s_name'].'</td>
							<td>'.$row['s_quantity'].'</td>
							<td>'.$row['s_receivedate'].'</td>
							<td>'.$row['s_vendor'].'</td>
							<td>
								<div class="btn-group">
									<a href="editstock.php?update&id='.$row['s_id'].'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
									<a href="stock_val.php?delete&stock_id='.$row['s_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Orders <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Order Date</th>
								<th>Customer Name</th>
								<th>Order NUmber</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `order`") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['o_date'].'</td>
							<td>'.$row['cust_name'].'</td>
							<td>'.$row['o_number'].'</td>
							<td>
								<div class="btn-group">
									<a href="order_details.php?order_id='.$row['o_id'].'&&order_number='.$row['o_number'].'" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
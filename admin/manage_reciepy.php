<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Recepie <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Item Name</th>
								<th>Description</th>
								<th>Ingradients</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT DISTINCT item_name, description FROM `reciepy`") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							$item_name = $row['item_name'];
							echo '<tr>
									<td>'.$i++.'</td>
									<td>'.$row['item_name'].'</td>
									<td>'.$row['description'].'</td>

									<td><ul class="nav">';
							$query = mysqli_query($con, "SELECT * FROM `reciepy` WHERE item_name = '$item_name'") or die(mysqli_error($con));
							while($rr = mysqli_fetch_array($query))
							{
								echo '<li>'.$rr['ingradients'].'-- '.$rr['qty'].''.$rr['in_terms'].'</li>
								<li class="devider"></li>';
									
							}

							echo '</ul></td>	<td>
										<a href="reciepy_val.php?delete&item_name='.$row['item_name'].'" class = "btn btn-danger btn-sm"><i class="fa fa-user fa-fw"></i>Delete</a>
									
										</td>
										</tr>';
							
						}
					
					?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">ADD NEW STOCK <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				<form method="post" action="stock_val.php">

					<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Stock Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Stock.....!</p>
						</div>';
					}
				?>
					
					<div class="form-group">
						<label>Stock Category </label>
						<select class="form-control"  data-validation="required" name="category">
							<option value="">---SELECT---</option>
							<?php
								include("connection.php"); 
								$sql = mysqli_query($con, "SELECT * FROM `category` WHERE cat_type = 'stock'") or die(mysqli_error($con));
								while($row = mysqli_fetch_array($sql))
								{
									echo '<option value="'.$row['cat_id'].'">'.$row['cat_name'].'</option>';
								}
							?>
						</select>
					</div>
			
					<div class="form-group">
						<label>Stock Name</label>
						<input type="text" id="name" name="name" data-validation="required" class="form-control" placeholder="Enter Stock Name" required>
					</div>
			
					<!--<div class="form-group">
						<label>Item Image</label>
					</div>-->
			
					<div class="form-group">
						<label>Quanitty</label>
						<input type="text" name="quantity" data-validation="required" class="form-control" placeholder="Enter the quantity" required>
					</div>

					<div class="form-group">
						<label>Received Date</label>
						<input type="date" name="date"  data-validation="required date" class="form-control" placeholder="Enter Received Date" required>
					</div>
					
					<div class="form-group">
						<label>Vendor Name</label>
						<input type="text" name="vendor" id="hhh" data-validation="required" class="form-control" placeholder="Enter Vendor Name" required>
					</div>

					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="ADD">
					</div>
				</form>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
            $( document ).ready(function() {
                $( "#hhh" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push" style="background:url(mayyia4.png); background-repeat: no-repeat; background-size: 100% 100%">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Add New Reciepy<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">

					<form method="post" action="reciepy_val.php">
					<div class="form-group">
						<label>Enter the Food Item</label>
						<input type="text" id="name" name="item" class="form-control" data-validation="required" placeholder="Enter the food item" required/>
					</div>

					<div class="form-group">
							<label> Description For Preparation </label>(<span id="maxlength">500</span> characters left)
							<textarea rows="4" name="description" class="form-control" id="area" data-validation="required" placeholder="Enter description of Preparation" required></textarea>
					</div>	
			
					<table data-validation="required" class="table table-bordered table-hover" id="tab_logic">
						<thead>
							<tr >
								<th class="text-center">
									Sno
								</th>
								<th class="text-center">
									Ingradients
								</th>
								<th class="text-center">
									Qty
								</th>
								<th class="text-center">
									In Terms
								</th>
							</tr>
						</thead>
						<tbody>
							<tr id='addr0'>
								<td>
								1
								</td>
								<td>
								<input type="text" data-validation="required" name='ingradient0' id="char"  placeholder='Item Name' class="form-control"/>
								</td>
								<td>
								<input type="number" data-validation="required" name='qty0' placeholder='Quantity' class="form-control"/>
								</td>
								<td>
								<select name="in_terms0" data-validation="required" class="form-control" required="">
									<option value="">--SELECT--</option>
									<option>Kgs</option>
									<option>Grams</option>
									<option>Litre</option>
									<option>Piece</option>
									<option>TSP</option>
								</select>
								</td>
							</tr>
		                    <tr id='addr1'></tr>
						</tbody>
					</table>
					<a id="add_row" class="btn btn-default pull-left">Add Row</a>
					<button type="submit" name="add_prod" class="btn btn-default text-center">Submit</button>
					<a id='delete_row' class="pull-right btn btn-default">Delete Row</a>
				</form>	
				</div>
			</div>
		</div>
	</div>
			
		<?php include("inc/footer.php"); ?>

		<script>
	
	     $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input name='ingradient"+i+"' type='text' placeholder='Item Name' class='form-control input-md'  /> </td><td><input  name='qty"+i+"' type='number' placeholder='Quantity'  class='form-control input-md'></td><td><select name='in_terms"+i+"' class='form-control' required=''><option>Kgs</option><option>Grams</option><option>Piece</option><option>Litre</option><option>TSP</option></select></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
    	 if(i>1){
		 $("#addr"+(i-1)).html('');
		 i--;
		 }
	 });

});
	
	</script>
	<script>
  			$('#area').restrictLength($('#maxlength'));
		</script>

		<script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

        <script>
            $( document ).ready(function() {
                $( "#char" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>
</body>
</html>
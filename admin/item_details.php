<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("connection.php");
			$i_id = $_GET['i_id'];

			$sql = mysqli_query($con, "SELECT * FROM `items` INNER JOIN `category` ON `items`.i_category = `category`.cat_id WHERE i_id = '$i_id'") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);

			$emp_profile= $row['i_image'];
			if($emp_profile == "")
			{
				$emp_profile = "images/user.png";
			}
			else
			{
				$emp_profile = "emp_profile/".$emp_profile;
			}
		?>
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Items<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-3">
							<div class="thumbnail">
								<img src="<?php echo $emp_profile; ?>" class="img-responsive img-circle">
							</div>
						</div>
						<div class="col-md-9">
							<table class="table">
								<tr>
									<th>Item Category</th>
									<td><?php echo $row['cat_name']; ?></td>
								</tr>

								<tr>
									<th>Item Name</th>
									<td><?php echo $row['i_name']; ?></td>
								</tr>

								<tr>
									<th>Item Subtitle</th>
									<td><?php echo $row['i_subtitle']; ?></td>
								</tr>

								<tr>
									<th>Unit Price </th>
									<td><?php echo $row['i_unitprice']; ?></td>
								</tr>

								<tr>
									<th>Item Prepared</th>
									<td><?php echo $row['i_prepared']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
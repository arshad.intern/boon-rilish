<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
			<?php include("inc/sidemenu.php"); ?>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<h3 class="title1">Manage Coupens <span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Coupen Title</th>
								<th>Coupen Code</th>
								<th>Coupen Discount</th>
								<th>Created Date</th>
								<th>Validity</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

							<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `coupens`") or die(mysqli_error($con));
						$i = 1;
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['c_title'].'</td>
							<td>'.$row['c_code'].'</td>
							<td>'.$row['c_discount'].'</td>
							<td>'.$row['c_createddate'].'</td>
							<td>'.$row['c_validity'].'</td>
							<td>
								<div class="btn-group">
									<a href="editcoupon.php?update&id='.$row['c_id'].'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
									<a href="coupen_val.php?delete&coupen_id='.$row['c_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
							</tr>';
						}
					
					?>

						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
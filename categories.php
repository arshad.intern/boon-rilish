<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

      <style type="text/css">
          img{max-width:100%;}
    *{transition: all .5s ease;-moz-transition: all .5s ease;-webkit-transition: all .5s ease}
.my-list {
    width: 100%;
    padding: 10px;
    border: 1px solid #f5efef;
    float: left;
    margin: 15px 0;
    border-radius: 5px;
    box-shadow: 2px 3px 0px #e4d8d8;
    position:relative;
    overflow:hidden;
}
.my-list h3{
    text-align: left;
    font-size: 14px;
    font-weight: 500;
    line-height: 21px;
    margin: 0px;
    padding: 0px;
    border-bottom: 1px solid #ccc4c4;
    margin-bottom: 5px;
    padding-bottom: 5px;
    }
    .my-list span{float:left;font-weight: bold;}
    .my-list span:last-child{float:right;}
    .my-list .offer{
    width: 100%;
    float: left;
    margin: 5px 0;
    border-top: 1px solid #ccc4c4;
    margin-top: 5px;
    padding-top: 5px;
    color: #afadad;
    }


     .my-list img{
         position: relative;
    width:  100%;
    height: 150px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    }
    .detail {
    position: absolute;
    top: -107%;
    left: 0;
    text-align: center;
    background: #fff;height: 100%;width:100%;
    
}
    
.my-list:hover .detail{top:0;}
      </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
    <div class="row"><br>
            <div class="well rg_form">
                <div class="agileits_w3layouts_head">
            <h3>Menu Items<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></h3>
            </div>
            <div class="w3layouts_gallery_grids">
                <?php
                    include("admin/connection.php");
                    $cat_id = $_GET['cat_id'];
                    $rr = mysqli_query($con, "SELECT * FROM `items` WHERE i_category = '$cat_id'") or die(mysqli_error($con));
                    $count = mysqli_num_rows($rr);

                    if($count > 0)
                    {
                        while ($row = mysqli_fetch_array($rr)) {
                            $image = $row['i_image'];
                            if($image == "")
                            {
                                $image = "";
                              # code...
                            }
                            else
                            {
                                $image = "admin/emp_profile/".$image;
                            }

                            echo '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="my-list">
                                        <img src="'.$image.'" alt="dsadas" />
                                        <h3>'.$row['i_name'].'</h3>
                                        <span>RS: '.$row['i_unitprice'].' [Per Piece]</span>
                                        <span class="pull-right">Qty: '.$row['i_prepared'].'</span>
                                        <div class="offer">'.$row['i_subtitle'].'</div>
                                        <div class="detail">
                                        <p>'.$row['i_name'].'</p>
                                        <img src="'.$image.'" alt="dsadas" />
                                        <a href="#" class="btn btn-info">Book Now</a>
                                        <a href="#" class="btn btn-info">Details</a>
                                        </div>
                                    </div>
                                </div>';

                        }
                    }
                    else
                    {
                        echo '<h3 style="color:red;" class="text-center">No Items Found...</h3>';
                    }
                    
                /*
                    include("admin/connection.php");
                    
                        echo '<div class="col-md-3 w3layouts_gallery_grid">
                                <a href="food_details.php?food_id='.$row[0].'" data-lsb-group="header">
                                    <div class="w3layouts_news_grid">
                                        <img src="'.$image.'" alt=" " class="img-responsive">
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text"><h3>'.$row['i_name'].'</h3></div>
                                        </div>
                                    </div>
                                </a>
                            </div>';
                      } 

                      */
                ?> 
                

                <div class="clearfix"> </div>
            </div>
        </div>
<!-- //gallery -->
<!-- gallery js -->
    <script src="js/lsb.min.js"></script>
    <script>
    $(window).load(function() {
          $.fn.lightspeedBox();
        });
    </script>


                
            </div>
	</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

</body>

</html>

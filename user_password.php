<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>

		<script>
function validatePassword() 
{
	var currentPassword,newPassword,confirmPassword,output = true;

	currentPassword = document.frmChange.currentPassword;
	newPassword = document.frmChange.newPassword;
	confirmPassword = document.frmChange.confirmPassword;

if(!currentPassword.value) 
{
	currentPassword.focus();
	document.getElementById("currentPassword").innerHTML = "required";
	output = false;
}
else if(!newPassword.value) 
{
	newPassword.focus();
	document.getElementById("newPassword").innerHTML = "required";
	output = false;
}
else if(!confirmPassword.value) 
{
	confirmPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "required";
	output = false;
}
if(newPassword.value != confirmPassword.value) 
{
	newPassword.value="";
	confirmPassword.value="";
	newPassword.focus();
	document.getElementById("confirmPassword").innerHTML = "not same";
	output = false;
} 	
return output;
}
</script>
</head> 
<body class="cbp-spmenu-push">

		<?php
		include("admin/connection.php");
		if(count($_POST)>0) {

        $result = "SELECT * from users WHERE email='$a_email'";
		$m=mysqli_query($con, $result);
		$row=mysqli_fetch_array($m);
		if($_POST["currentPassword"] == $row["password"]) {
		$e="UPDATE users set password='" . $_POST["newPassword"]. "' WHERE email='$a_email'";
		$res=mysqli_query($con, $e);
		$message = "Password Changed";
		} else $message = "Current Password is not correct";
		}
	?>
	<div class="main-content">
		<!--left-fixed -navigation-->
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu2.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div class="container">
		<div class="row"><br>
			<div class="col-md-12" style="margin-top:5%; margin-bottom:12%;">
			<div class="well rg_form">
				<center><h3>CHANGE PASSWORD<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3></center>
				<hr/>

						<?php
					if(isset($_GET['success']))
					{
						echo'<div class="alert alert-success">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Success.....!</b>Coupen Added Successfully....!</p>
						</div>';
					}
					else if(isset($_GET['error']))
					{
						echo'<div class="alert alert-danger">
						<a herf="#" class="close" data-dismiss="alert">&times;</a>
						<p><b>Error.....!</b>Error while Adding Coupen.....!</p>
						</div>';
					}
				?>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
				
							<form name="frmChange" method="post" action="" onSubmit="return validatePassword()">

								<?php if(isset($message)) {
									echo '<div class="alert alert-info">'.$message.'</div>'; 
									}
								?>
								<table class="table table-hover">
									
									<tr>
										<td width="40%"><label> Current Password </label></td>
										<td width="60%"><input type="password" name="currentPassword" data-validation="required" class="form-control"/><span id="currentPassword"  class="required"></span></td>
									</tr>
									<tr>
										<td><label> New Password </label></td>
										<td><input type="password" name="newPassword" data-validation="required" class="form-control"/><span id="newPassword" class="required"></span></td>
									</tr>
									<tr>
										<td><label> Confirm Password </label></td>
										<td><input type="password" name="confirmPassword" data-validation="required" class="form-control"/><span id="confirmPassword" class="required"></span></td>
									</tr>
									<tr>
										<center><td colspan="2"><input type="submit" name="submit" value="Submit" class="btn btn-primary btn-block"></td></center>
									</tr>
								</table>
							</form>
						</div>

				</div>
			</div>
		</div>
		</div></div>
		<?php include("inc/footer.php"); ?>
</body>
</html>
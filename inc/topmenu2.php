<div class="banner jarallax">
        <div class="agileinfo-dot">
            <div class="header">
                <div class="container-fluid">
                    <div class="header-left">
                        <div class="w3layouts-logo grid__item">
                            <h1>
                                <a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
                            </h1>
                        </div>
                    </div>
                    <div class="top-nav">
                        <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                
                                    <?php
                                        if(isset($_SESSION['email']))
                                        {
                                            echo '
                                            <li><a class="active" href="home.php">Home</a></li>
                                                    <li><a href="gallary.php">Gallery</a></li>
                                                    <li><a href="food_items.php">Menu Items</a></li>
                                                    <li><a href="orderform.php">Order</a></li>
                                                    <li><a href="feedback.php">Feedback</a></li>     
                                        

                                            <li class="dropdown">
                                                    <a href="" class="dropdown-toggle" data-toggle="dropdown">Welcome,'.$user_name.'<span class="caret"></span></a>
                                                    <ul class="dropdown-menu" style="background-color:#333;">
                                                    <li><a href="myorder.php">&nbsp;&nbsp;&nbsp;My Orders</a></li>
                                                        <li><a href="user_profile.php">View Profile</a></li>
                                                        <li><a href="user_password.php">Change Password</a></li>
                                                        <li><a href="index.php">Logout</a></li>
                                                    </ul>
                                                </li>';
                                        }
                                        else
                                        {
                                            echo '
                                            <li><a class="active" href="index.php">Home</a></li>
                                                    <li><a href="#about" class="scroll">About</a></li>
                                                    <li><a href="#services" class="scroll">Services</a></li>
                                                    <li><a href="#chefs" class="scroll">Chefs</a></li>
                                                    <li><a href="#gallery" class="scroll">Gallery</a></li>  
                                                    <li><a href="#customer" class="scroll">Customers</a></li>
                                                    <li><a href="#contact" class="scroll">Contact</a></li>
                                                    <li><a href="user_login.php">Login</a></li>
                                                    <li><a href="user_reg.php">Register</a></li>';
                                        }
                                    ?>
                                    
                                </ul>
                        
                                <div class="clearfix"> </div>                       
                            </div>  
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        </nav>      
                </div>      
            </div>
        </div>
<?php include("inc/session.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<!--left-fixed -navigation-->
		<!-- header-starts -->
			<?php include("inc/topmenu2.php"); ?>
		<!-- //header-ends -->
		<!-- main content start-->

		<?php
			include("admin/connection.php");
			$i_id = $_GET['i_id'];

			$sql = mysqli_query($con, "SELECT * FROM `items` WHERE i_id = '$i_id'") or die(mysqli_error($con));
			$row = mysqli_fetch_array($sql);

			$emp_profile= $row['i_image'];
		
				$emp_profile = "admin/emp_profile/".$emp_profile;
		?>
		<div class="container">
		<div class="row"><br>
			<div class="col-md-12" style="margin-top:5%; margin-bottom:12%;">
			<div class="well rg_form">
				<h3 class="title1">Item Details<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3><br>
				<div class="blank-page widget-shadow scroll" id="style-2 div1">
					<div class="row">
						<div class="col-md-3">
							<div class="thumbnail">
								<img src="<?php echo $emp_profile; ?>" class="img-responsive img-circle">
							</div>
						</div>
						<div class="col-md-9">
							<table class="table">
								<tr>
									<th>Item Category</th>
									<td><?php echo $row['i_category']; ?></td>
								</tr>

								<tr>
									<th>Item Name</th>
									<td><?php echo $row['i_name']; ?></td>
								</tr>

								<tr>
									<th>Item Subtitle</th>
									<td><?php echo $row['i_subtitle']; ?></td>
								</tr>

								<tr>
									<th>Unit Price </th>
									<td><?php echo $row['i_unitprice']; ?></td>
								</tr>

								<tr>
									<th>Item Prepared</th>
									<td><?php echo $row['i_prepared']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			</div>
		</div>
		
		<?php include("inc/footer.php"); ?>
</body>
</html>
<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

      <style type="text/css">
          img{max-width:100%;}
    *{transition: all .5s ease;-moz-transition: all .5s ease;-webkit-transition: all .5s ease}
.my-list {
    width: 100%;
    padding: 10px;
    border: 1px solid #f5efef;
    float: left;
    margin: 15px 0;
    border-radius: 5px;
    box-shadow: 2px 3px 0px #e4d8d8;
    position:relative;
    overflow:hidden;
}
.my-list h3{
    text-align: left;
    font-size: 14px;
    font-weight: 500;
    line-height: 21px;
    margin: 0px;
    padding: 0px;
    border-bottom: 1px solid #ccc4c4;
    margin-bottom: 5px;
    padding-bottom: 5px;
    }
    .my-list span{float:left;font-weight: bold;}
    .my-list span:last-child{float:right;}
    .my-list .offer{
    width: 100%;
    float: left;
    margin: 5px 0;
    border-top: 1px solid #ccc4c4;
    margin-top: 5px;
    padding-top: 5px;
    color: #afadad;
    }


    .my-list img{
         position: relative;
    width:  100%;
    height: 150px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    }
    .detail {
    position: absolute;
    top: -107%;
    left: 0;
    text-align: center;
    background: #fff;height: 100%;width:100%;
    
}
    
.my-list:hover .detail{top:0;}

.h3
{
    font-color: white;
    font-weight: bold;

}
      </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
		<div class="gallery" id="gallery">
            <div class="agileits_w3layouts_head">
                <div class="well">
                <h3>Menu Items<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3>
                <center><h4><b>NOTE: HERE YOU CAN ORDER ONLY ONE ITEM, IF YOU WANT TO ORDER MORE ITEMS THEN SELECT <a href="orderform.php"><u>ORDER</u></a> OPTION. </b></h4></center>
                </div>
            </div>
            <div class="w3layouts_gallery_grids">
                <?php
                    include("admin/connection.php");
                    $sql = mysqli_query($con, "SELECT * FROM `category` WHERE cat_type = 'menu'") or die(mysqli_error($con));
					$j = 0;
					 $i = 1;
                    while($tt = mysqli_fetch_array($sql))
                    {
						
					
                        echo '<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>'.$tt['cat_name'].'<span class="pull-right"><a class=class="wthree_text" href="categories.php?cat_id='.$tt['cat_id'].'">MORE</a></span></h4>
                    </div>

                    <div class="panel-body">';
                    $cat_id = $tt[0];
                   
                    $rr = mysqli_query($con, "SELECT * FROM `items` WHERE i_category = '$cat_id' LIMIT 4") or die(mysqli_error($con));
                    while ($row = mysqli_fetch_array($rr)) 
                    {
						$i++;
                        $image = $row['i_image'];
                        if($image == "")
                        {
                            $image = "";
                          # code...
                        }
                        else
                        {
                            $image = "admin/emp_profile/".$image;
                        }

                        echo 
                            '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="my-list">
                                    <img src="'.$image.'" alt="dsadas" />
                                    <h3>'.$row['i_name'].'</h3>
                                    <span>RS: '.$row['i_unitprice'].' [Per '.$row['i_measure'].']</span>
                                    <span class="pull-right">Qty: '.$row['i_prepared'].'</span>
                                    <div class="offer">'.$row['i_subtitle'].'</div>
                                    <div class="detail">
                                    <p>'.$row['i_name'].'</p>
                                    <img src="'.$image.'" alt="dsadas" />
                                    <a href="#"  data-toggle="modal" data-target="#myModal'.$i.'" class="btn btn-info">Order Now</a>
                                    <a href="item_detail.php?i_id='.$row[0].'"  name="detail" class="btn btn-info">Details</a>
                                    </div>
                                </div>
                            </div>



                            <!-- Modal -->
                        <div id="myModal'.$i.'" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!--Modal Content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Order '.$row['i_subtitle'].'</h4>
                              </div>
                              <div class="modal-body">
                                <form name="" method="post"  action="food_items_val.php">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Measuring Term:</label>
                                                <input type="text" name="measure" class="form-control" required readonly value="'.$row['i_measure'].'">
                                                <input type="hidden" name="item_name" value="'.$row['i_name'].'"/>
                                            </div>
                                        </div>

                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Select Quantity:</label>
                                                <select name="quantity" class="form-control" id="qty'.$i.'" required>';
                                                for($k = 1; $k <= $row['i_prepared']; $k++)
                                                {
                                                    echo '<option>'.$k.'</option>';
                                                   
                                                }
                                                echo '</select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Price Per Piece</label>
                                                <input type="text" name="per" id="price'.$i.'" class="form-control" required readonly value="'.$row['i_unitprice'].'"/>
                                            </div>
                                        </div>

                                      <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Order Date</label>
                                                <input type="date" data-valiadtion="required" name="date" min='.date('Y-m-d').' class="form-control" required value=""/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">    
                                                <label>Total Price</label>
                                                <input type="text" id="total_price'.$i.'" name="total" class="form-control" required readonly/>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="color:red;" id="error'.$i.'"></div>
                            </div>
                            <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" name="order_i" class="btn btn-primary btn-block">Order Item</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="food_items.php" class="btn btn-primary btn-block" data-dismiss="modal">Close</a>
                                </div>

                                
                            </div>
                              </form>
							  </div>
							
                            </div>

                          </div>
                        </div>


                        <script type="text/javascript">
                            $(document).ready(function(){
                                $("#qty'.$i.'").change(function(){
                                    var qty = $(this).val();
                                    var mesasure_term = $("#subramanya'.$i.'").val();
                                    var price = $("#price'.$i.'").val();
                                    //alert(mesasure_term);
                                    if(qty){
                                        $.ajax({
                                            type:"POST",
                                            url:"calculate_price.php",
                                            data:"mesasure_term="+mesasure_term+"&qty="+qty+"&price="+price,
                                            success:function(html){
                                                //alert(html.total_price);
                                                $("#total_price'.$i.'").val(html.total_price);

                                                $("#error'.$i.'").html(html.error);
                                            },
                                             error: function (error) {
                                              
                                            }
                                        }); 
                                    }else{
                                      
                                    }
                                });
                            });
                            </script>';

                       
                    }



                    echo '


                    </div>
                </div>';
                 $i++;

                    }                
                /*
                    include("admin/connection.php");
                    
                        echo '<div class="col-md-3 w3layouts_gallery_grid">
                                <a href="food_details.php?food_id='.$row[0].'" data-lsb-group="header">
                                    <div class="w3layouts_news_grid">
                                        <img src="'.$image.'" alt=" " class="img-responsive">
                                        <div class="w3layouts_news_grid_pos">
                                            <div class="wthree_text"><h3>'.$row['i_name'].'</h3></div>
                                        </div>
                                    </div>
                                </a>
                            </div>';
                      } 

                      */
                ?> 
                

                <div class="clearfix"> </div>
            </div>
        </div>
<!-- //gallery -->
<!-- gallery js -->
    <script src="js/lsb.min.js"></script>
    <script>
    $(window).load(function() {
          $.fn.lightspeedBox();
        });
    </script>
	</div>
</div>
    <!-- /.container -->

 
    <?php ///include("inc/footer.php"); ?>
    

</body>

</html>

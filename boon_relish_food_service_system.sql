-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2020 at 01:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boon_relish_food_service_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cat_id` int(15) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `cat_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat_id`, `cat_name`, `cat_type`) VALUES
(1, 'Snacks', 'menu'),
(3, 'Breakfast', 'menu'),
(4, 'Special Foods', 'menu'),
(5, 'Powder', 'stock'),
(6, 'Vessels', 'stock'),
(7, 'Tissue Paper', 'stock'),
(10, 'Chair', 'stock'),
(11, 'MOMOS', 'stock'),
(12, 'bevrge', 'menu');

-- --------------------------------------------------------

--
-- Table structure for table `coupens`
--

CREATE TABLE `coupens` (
  `c_id` int(15) NOT NULL,
  `c_title` varchar(30) NOT NULL,
  `c_code` varchar(30) NOT NULL,
  `c_discount` bigint(30) NOT NULL,
  `c_createddate` date NOT NULL,
  `c_validity` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupens`
--

INSERT INTO `coupens` (`c_id`, `c_title`, `c_code`, `c_discount`, `c_createddate`, `c_validity`) VALUES
(1, 'ugadi', 'AD5896', 7, '2018-03-24', '2018-04-21');

-- --------------------------------------------------------

--
-- Table structure for table `debit_card`
--

CREATE TABLE `debit_card` (
  `debit_id` int(15) NOT NULL,
  `debit_name` varchar(50) NOT NULL,
  `debit_number` bigint(20) NOT NULL,
  `exp_month` int(20) NOT NULL,
  `exp_year` year(4) NOT NULL,
  `cv_number` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debit_card`
--

INSERT INTO `debit_card` (`debit_id`, `debit_name`, `debit_number`, `exp_month`, `exp_year`, `cv_number`) VALUES
(7, 'Subrahmanya Bhat', 5103720037727722, 6, 2021, 480),
(8, 'Raviteja', 1234567891234567, 3, 2021, 123),
(9, 'Manamohan', 9876543211234567, 3, 2021, 456),
(10, 'Raviteja', 1234567891234567, 3, 2021, 123);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `e_id` int(11) NOT NULL,
  `e_name` varchar(50) NOT NULL,
  `e_phone` bigint(50) NOT NULL,
  `e_gender` text NOT NULL,
  `e_address` varchar(50) NOT NULL,
  `e_email` varchar(50) NOT NULL,
  `e_doj` date NOT NULL,
  `e_experience` varchar(50) NOT NULL,
  `e_adhar` bigint(50) NOT NULL,
  `e_photo` varchar(30) NOT NULL,
  `e_password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`e_id`, `e_name`, `e_phone`, `e_gender`, `e_address`, `e_email`, `e_doj`, `e_experience`, `e_adhar`, `e_photo`, `e_password`) VALUES
(1, 'Manu k', 7090572969, 'Male', '		udupi				', 'manu@gmali.com', '2018-03-12', '05', 829464685553, '80108-chef3.jpg', 'emp-163'),
(2, 'Rajesh', 9564875112, 'Male', 'mangalore', 'rajesh@gmail.com', '1998-03-23', '3', 345237868425, '36675-chef2.jpg', 'emp-856'),
(3, 'Rajesh', 8965478596, 'Male', 'mangalore', 'rajesh1@gmail.com', '2018-04-02', '8', 789654123123, '70628-chef2.jpg', 'emp-694'),
(4, 'Thanu', 9900990099, 'Female', 'Bangalore', 'thanu123@gmail.com', '2018-04-02', '3', 123456789121, '19801-about.png', 'emp-832');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `f_id` int(15) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `f_email` varchar(30) NOT NULL,
  `f_num` bigint(20) NOT NULL,
  `f_message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`f_id`, `f_name`, `f_email`, `f_num`, `f_message`) VALUES
(3, 'Manu', 'manu@gmail.com', 8765321452, 'Good and tasty food'),
(4, 'Thanu', 'thanu@gmail.com', 8762313835, 'Bangalore'),
(5, 'Raja', 'raja@gmail.com', 986532144, 'Mangalore'),
(6, 'Aysha', 'aysha@gmail.com', 651320, 'kalskasn as');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `i_id` int(11) NOT NULL,
  `i_category` varchar(50) NOT NULL,
  `i_name` varchar(50) NOT NULL,
  `i_subtitle` varchar(50) NOT NULL,
  `i_image` varchar(50) NOT NULL,
  `i_measure` varchar(20) NOT NULL,
  `i_unitprice` bigint(50) NOT NULL,
  `i_prepared` bigint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`i_id`, `i_category`, `i_name`, `i_subtitle`, `i_image`, `i_measure`, `i_unitprice`, `i_prepared`) VALUES
(6, 'Snacks', 'bonda', 'Potato bonda', '67265-bonda.jpg', 'Piece', 20, 200),
(7, '3', 'Palav', 'veg palav', '96028-palav2.jpg', 'KG', 20, 200),
(8, '3', 'idli', 'rava idli', '67764-rava-idli2.jpg', 'Piece', 20, 200),
(9, '3', 'Puri-Bhaji', 'Puri-Bhaji', '71830-puri-sagu2.jpg', 'Piece', 30, 300),
(12, '4', 'Baby Corn Chilli ', 'Chilli', '11562-baby-corn-masala.jpg', 'KG', 60, 20),
(13, '4', 'Baby corn Manchurian ', 'Manchurian ', '31393-baby_corn_manchurian.jpg', 'KG', 70, 20),
(14, '3', 'buns', 'buns', '32058-buns2.jpg', 'Piece', 20, 30),
(16, '3', 'dosa', 'plain dosa', '94514-plain_dosa.jpg', 'Piece', 30, 40),
(17, '1', 'Biscut Rotti', 'biscut rotti', '89100-biscut_rotti2.jpg', 'Piece', 30, 50),
(18, '1', 'Chattambade', 'Chattambade', '75253-chattambade2.jpg', 'Piece', 20, 60),
(19, '1', 'Cutlet', 'Cutlet', '61845-cutlet2.jpg', 'Piece', 20, 50),
(20, '4', 'Baby corn manchurian', 'baby corn manchurian', '57485-baby_corn_manchurian2.jpg', 'KG', 50, 20),
(21, '4', 'Chilli Mashroom', 'Chilli Mashroom', '44799-chilli-mushroom2.jpg', 'KG', 40, 30),
(24, '1', 'kharjikai', 'KharjiKai', '35545-kharjiaki2.jpg', 'Piece', 15, 100),
(25, '8', 'puff', 'Veg-Puffs', '99091-panner-masala2.jpg', 'Piece', 12, 200),
(26, '1', 'Jeegujje Podi', 'Jackfruit fry', '18290-jeegujje-podi2.jpg', 'Piece', 5, 50),
(27, '1', 'kichdi', 'nkjlds', '61135-1.jpg', 'Grams', 45, 2);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `u_id` int(15) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(20) NOT NULL,
  `gender` text NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` bigint(30) NOT NULL,
  `a_photo` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `user_role` int(10) NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`u_id`, `name`, `email`, `gender`, `address`, `phone`, `a_photo`, `password`, `user_role`, `status`) VALUES
(1, 'Ravichandra', 'admin@gmail.com', 'male', 'Mangalore', 9900990099, '', 'admin123', 1, 1),
(2, 'Manu', 'manu@gmali.com', 'Male', '		udupi				', 7090572969, '90193-chef2.jpg', 'emp-843', 2, 1),
(3, 'dzfdx', 'ravi@gmail.com', 'Male', 'zvsddfbdfgnfhnfghmfghm', 414141, '36675-chef2.jpg', 'ravi123', 2, 1),
(4, 'Rajesh', 'raj@gmail.com', 'Male', 'mangalore', 8965478596, '70628-chef2.jpg', 'emp-350', 2, 1),
(5, 'Thanu', 'thanu123@gmail.com', 'Male', 'Bangalore', 8762313835, '19801-about.png', 'emp-521', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(15) NOT NULL,
  `menu_title` varchar(50) NOT NULL,
  `menu_subtitle` varchar(50) NOT NULL,
  `item_image` varchar(30) NOT NULL,
  `menu_price` bigint(10) NOT NULL,
  `menu_quant` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_order`
--

CREATE TABLE `menu_order` (
  `mo_id` int(15) NOT NULL,
  `item_name` varchar(35) NOT NULL,
  `mo_measure` varchar(50) NOT NULL,
  `mo_qty` bigint(30) NOT NULL,
  `mo_price` bigint(30) NOT NULL,
  `order_date` date NOT NULL,
  `total` bigint(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_order`
--

INSERT INTO `menu_order` (`mo_id`, `item_name`, `mo_measure`, `mo_qty`, `mo_price`, `order_date`, `total`) VALUES
(1, 'Chattambade', 'Piece', 2, 20, '2018-04-05', 40),
(2, 'Chattambade', 'Piece', 2, 20, '2018-04-05', 40),
(3, 'Cutlet', 'Piece', 2, 20, '2018-04-05', 40),
(4, 'Cutlet', 'Piece', 2, 20, '2018-04-05', 40),
(5, 'Cutlet', 'Piece', 2, 20, '2018-04-05', 40),
(6, 'Chattambade', 'Piece', 2, 20, '2018-04-05', 40),
(7, 'Biscut Rotti', 'Piece', 3, 30, '2018-04-05', 90),
(8, 'Chattambade', 'Piece', 2, 20, '2018-04-05', 40),
(9, 'Biscut Rotti', 'Piece', 1, 30, '2020-05-17', 0),
(10, 'Baby corn Manchurian ', 'KG', 1, 70, '2020-05-17', 70),
(12, 'Biscut Rotti', 'Piece', 1, 30, '2020-05-17', 30),
(13, 'Chattambade', 'Piece', 2, 20, '2020-05-17', 40),
(14, 'Chattambade', 'Piece', 2, 20, '2020-05-17', 40),
(15, 'Chattambade', 'Piece', 2, 20, '2020-05-17', 40),
(17, 'Baby corn Manchurian ', 'KG', 2, 70, '2020-05-17', 140);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `o_id` int(15) NOT NULL,
  `o_number` varchar(30) NOT NULL,
  `c_code` varchar(30) NOT NULL,
  `o_date` date NOT NULL,
  `cust_name` varchar(30) NOT NULL,
  `i_category` varchar(30) NOT NULL,
  `item_name` varchar(30) NOT NULL,
  `unitprice` int(20) NOT NULL,
  `remain_qty` bigint(20) NOT NULL,
  `req_qty` varchar(20) NOT NULL,
  `total` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`o_id`, `o_number`, `c_code`, `o_date`, `cust_name`, `i_category`, `item_name`, `unitprice`, `remain_qty`, `req_qty`, `total`) VALUES
(2, 'BR-201803232', 'AS3456', '2018-03-24', 'ram', '4', 'Baby corn Manchurian ', 70, 20, '10', 700),
(4, 'BR-201803312', '', '2018-03-31', 'Kannan', '1', 'Biscut Rotti', 30, 50, '10', 300),
(5, 'BR-201804034', 'AS3456', '2018-04-03', 'Kharjikai', '1', 'bonda', 299, 200, '10', 2990),
(6, 'BR-201804036', 'AS3456', '2018-04-04', 'Karan', '3', 'Puri-Bhaji', 30, 300, '10', 300),
(7, 'BR-201804043', '', '2018-04-06', 'shiva', '2', 'Meals', 40, 30, '10', 400),
(8, 'BR-201804044', 'AS3456', '2018-04-05', 'Rajesh', '3', 'Palav', 20, 200, '2', 40),
(9, 'BR-201804053', '', '0000-00-00', 'amar', '1', 'Biscut Rotti', 30, 50, '10', 300),
(10, 'BR-201804053', '', '2018-04-06', 'Raja', '1', 'Chattambade', 20, 60, '10', 200),
(11, 'BR-201804055', '', '2018-04-06', 'Preksha', '4', 'Baby corn manchurian', 70, 20, '2', 140),
(12, 'BR-201804055', 'AD5896', '2018-04-06', 'Preksha ', '4', 'Chilli Mashroom', 40, 30, '2', 80),
(13, 'BR-201804054', '', '2018-04-21', 'gdgd', '3', 'idli', 20, 200, '44', 880),
(14, 'BR-201804054', '', '2018-04-20', 'tata', '1', 'Jeegujje Podi', 5, 50, '7', 35),
(15, 'BR-201804055', 'AD5896', '2018-04-07', 'Ravichandra', '3', 'Puri-Bhaji', 30, 300, '8', 240),
(16, 'BR-2018040510', '', '2018-04-20', 'Ravichandra', '3', 'Palav', 20, 200, '7', 140),
(17, 'BR-202005179', 'ugadi', '2020-05-17', 'aysha', '3', 'Palav', 20, 200, '1', 20);

-- --------------------------------------------------------

--
-- Table structure for table `reciepy`
--

CREATE TABLE `reciepy` (
  `reciepy_id` int(15) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `description` varchar(500) NOT NULL,
  `qty` bigint(30) NOT NULL,
  `ingradients` varchar(30) NOT NULL,
  `in_terms` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reciepy`
--

INSERT INTO `reciepy` (`reciepy_id`, `item_name`, `description`, `qty`, `ingradients`, `in_terms`) VALUES
(23, 'Potato Bonda', 'Boil potatoes and make it pieces and make it balls & fry it.', 50, 'Atta', 'Grams'),
(24, 'Potato Bonda', 'Boil potatoes and make it pieces and make it balls & fry it.', 2, 'Potato', 'Piece'),
(25, 'Potato Bonda', 'Boil potatoes and make it pieces and make it balls & fry it.', 1, 'Chilli Powder', 'TSP'),
(26, 'Potato Bonda', 'Boil potatoes and make it pieces and make it balls & fry it.', 1, 'Salt', 'TSP'),
(27, 'Potato Bonda', 'Boil potatoes and make it pieces and make it balls & fry it.', 2, 'Oil', 'Litre'),
(33, 'Puffs ', 'Eat with freshness and have a good day', 50, 'atta', 'Grams'),
(34, 'Puffs ', 'Eat with freshness and have a good day', 1, 'potato', 'Piece'),
(35, 'Puffs ', 'Eat with freshness and have a good day', 1, 'salt', 'TSP'),
(36, 'Puffs ', 'Eat with freshness and have a good day', 1, 'Onion ', 'Piece'),
(37, 'Puffs ', 'Eat with freshness and have a good day', 1, 'termeric powder', 'TSP'),
(38, 'Puffs ', 'Eat with freshness and have a good day', 2, 'oil', 'Litre');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `s_id` int(15) NOT NULL,
  `s_category` int(11) NOT NULL,
  `s_name` varchar(30) NOT NULL,
  `s_quantity` varchar(30) NOT NULL,
  `s_receivedate` date NOT NULL,
  `s_vendor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`s_id`, `s_category`, `s_name`, `s_quantity`, `s_receivedate`, `s_vendor`) VALUES
(1, 6, 'Kharjikai', '1', '2018-04-19', 'Ashok Delars'),
(2, 1, 'pots', '25', '2018-03-25', 'prakash store'),
(3, 2, 'Oil', '10', '2018-04-02', 'Karuna stores'),
(7, 3, 'hande', '1', '2018-04-03', 'More '),
(8, 4, 'Milk Powder', '10 Packs', '2018-04-03', 'Namm ');

-- --------------------------------------------------------

--
-- Table structure for table `stock_category`
--

CREATE TABLE `stock_category` (
  `st_id` int(15) NOT NULL,
  `st_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gender` text NOT NULL,
  `phone` bigint(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `gender`, `phone`, `address`, `password`, `status`) VALUES
(1, 'Ravichandra', 'ravi@gmail.com', 'Male', 9900990099, 'Mangalore', 'arshad123', 1),
(2, 'Raja', 'raja@gmail.com', 'Male', 9900990055, 'Mysore', 'raja123456', 1),
(3, 'Jayaram', 'jay@gmail.com', 'Male', 8762313831, 'Mangalore', 'jay123456', 1),
(6, 'manu', 'manu123@gmail.com', 'Male', 9865321470, 'Mangalore', 'manu123456789', 1),
(8, 'Manu', 'manu111@gmail.com', 'Male', 8796547852, 'Mangalore', 'manu123456789', 1),
(9, 'Subrahmanya', 'ttfbsubbu273@gmail.com', 'Male', 8762313835, 'Honnavara', 'subbu123456', 1),
(10, 'arshad', 'a@gmail.com', 'Male', 9448348128, 'kalasa', 'arshad123', 1),
(11, 'aysha', 'aysha@gmail.com', 'Male', 9448348128, 'kalasa', 'aysha123', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `coupens`
--
ALTER TABLE `coupens`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `debit_card`
--
ALTER TABLE `debit_card`
  ADD PRIMARY KEY (`debit_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `menu_order`
--
ALTER TABLE `menu_order`
  ADD PRIMARY KEY (`mo_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `reciepy`
--
ALTER TABLE `reciepy`
  ADD PRIMARY KEY (`reciepy_id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `stock_category`
--
ALTER TABLE `stock_category`
  ADD PRIMARY KEY (`st_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cat_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `coupens`
--
ALTER TABLE `coupens`
  MODIFY `c_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `debit_card`
--
ALTER TABLE `debit_card`
  MODIFY `debit_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `f_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `u_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_order`
--
ALTER TABLE `menu_order`
  MODIFY `mo_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `o_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `reciepy`
--
ALTER TABLE `reciepy`
  MODIFY `reciepy_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `s_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `stock_category`
--
ALTER TABLE `stock_category`
  MODIFY `st_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include("inc/session.php"); ob_start();?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
	
		   <style>
		.donate-now {
     list-style-type:none;
     margin:25px 0 0 0;
     padding:0;
	 overflow:hidden;
}

.donate-now li {
     float:left;
     margin:0 5px 0 0;
    width:47%;
    height:auto;
}

.donate-now label, .donate-now input {
    display:block;
    top:0;
    left:0;
    right:0;
    bottom:0;
}

.donate-now input[type="radio"] {
    opacity:0.01;
    z-index:100;
}

.donate-now input[type="radio"]:checked + label,
.Checked + label {
    background:yellow;
}

.donate-now label {
     padding:50px;
	 text-align:center;
     border:1px solid #CCC; 
     cursor:pointer;
    z-index:90;
}

.donate-now label:hover {
     background:#fff;
	 box-shadow:3px 3px 25px #333;
	 border-radius:13px;
}
</style>

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
		<div class="col-md-12" style="margin-bottom:10%;">
			<div class="well rg_form">
				<center><h3>ORDER CONFIRMATION<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3></center>
				<hr/>
				<table class="table table-bordered">
						<thead>
							<tr>
								<th>Sno</th>
								<th>Order Number</th>
								<th>Coupon Code</th>
								<th>Order Date</th>
								<th>Custoomer Name</th>
								<th>Item Category</th>
								<th>Item Name</th>
								<th>Unit Price</th>
								<th>Required Quantity</th>
								<th>Total Price</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>

						<?php
						include("connection.php");
						$sql = mysqli_query($con, "SELECT `o_id`, `o_number`, `c_code`, `o_date`, `cust_name`, `i_category`, `item_name`, `unitprice`,`req_qty`, `total` FROM `order` WHERE `o_number` = '".$_SESSION['order_no']."'") or die(mysqli_error($con));
						$i = 1;
						$order_no = 0;
						$cust_name = "";
						$o_date="";
						$c_code="";
						while($row = mysqli_fetch_array($sql))
						{
							echo '<tr>
							<td>'.$i++.'</td>
							<td>'.$row['o_number'].'</td>
							<td>'.$row['c_code'].'</td>
							<td>'.$row['o_date'].'</td>
							<td>'.$row['cust_name'].'</td>
							<td>'.$row['i_category'].'</td>
							<td>'.$row['item_name'].'</td>
							<td>'.$row['unitprice'].'</td>
							<td>'.$row['req_qty'].'</td>
							<td>'.$row['total'].'</td>

							<td>
								<div class="btn-group">
								<a href="editorder.php?update&id='.$row['o_id'].'" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
								<a href="order_food_val.php?delete&oid='.$row['o_id'].'" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
							
							</tr>';

							$order_no = $row['o_number'];

							$cust_name = $row['cust_name'];
							$o_date=$row['o_date'];
							$c_code=$row['c_code'];
						}
					?>
						</tbody>
					</table>

					<?php
					if(isset($_POST['payment_method']))
					{
						header("location:payment.php?order_no=".$order_no."&&cust_name=".$cust_name."&o_date=".$o_date."&c_code=".$c_code);
					}
					else if(isset($_POST['payment_method1']))
					{
						header("location:bill_inovice.php?order_no=".$order_no."&&cust_name=".$cust_name."&o_date=".$o_date."&c_code=".$c_code);
					}
				?>

				<div class="row">
					<center><h4>SELECT THE PAYMENT METHOD<span class="pull-right"></span></h4></center>
					<div class="col-md-6 col-md-offset-4">	
						<form name="" method="post" action="" id="myform">
						<ul class="donate-now">
					
						<li>
							<input type="radio" id="payment_method1" name="payment_method1" value="b">
							<label for="payment_method1"><i class="fa fa-dollar fa-5x"></i><br>Cash Payment</label>
						</li>
						<li>
							<input type="radio" id="payment_method" name="payment_method" value="a" >
							<label for="payment_method"><i class="fa fa-globe fa-5x"></i><br>Online Payment</label>
						</li>
						</ul>
						</form>
					</div>
				<script>
					$('input[type=radio]').change(function(){
					$('#myform').submit();
					});
				</script>
			</div>
			</div>
		</div>
	</div>
	</div>
</div>

    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

</body>

</html>

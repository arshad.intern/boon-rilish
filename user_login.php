<?php include("inc/session.php"); ob_start();?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
		<div class="banner jarallax">
		<div class="agileinfo-dot">
			<div class="header">
				<div class="container-fluid">
					<div class="header-left">
						<div class="w3layouts-logo grid__item">
							<h1>
								<a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
							</h1>
						</div>
					</div>
					<div class="top-nav">
						<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav">

								
									<?php
											echo '
													<li><a href="user_login.php">Login</a></li>
													<li><a href="user_reg.php">Register</a></li>';
									?>
									
								</ul>
						
								<div class="clearfix"> </div>						
							</div>	
						</nav>
					</div>		
				</div>		
			</div>
		</div>
</div>
<div class="container">
	<div class="row"><br>
		<div class="col-md-6 col-md-offset-3">
			<div class="well login_form">
				<center><h4>LOGIN</h4></center>
				<hr/>
				<form method="post" action="">
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" placeholder="Enter Your Email" required/>
					</div>
			
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Enter Your Password" required>
					</div>
					
					<div class="form-group">
						<input type="submit" name="log_btn" class="btn btn-primary btn-block" value="Login">
					</div>
				</form>
				<?php 
					if(isset($_POST['log_btn']))
					{
						$email = $_POST['email'];
						$password = $_POST['password'];

						include("admin/connection.php");
						$sql = mysqli_query($con, "SELECT * FROM `users` WHERE email = '$email' AND password = '$password'") or die(mysqli_error($con));
						$row = mysqli_fetch_array($sql);
						if($row)
						{
							session_start();
							$_SESSION['email'] = $row['email'];
							header("location:home.php?logged_in");
						}
						else
						{
							echo '<h4 style="color:red; text-align:center;">Invalid Email and Password...</h4>';
						}
					}
					
				?>
				<p>Didn't Register Yet? <a href="user_reg.php">Register Now </a></p>
			</div>
		</div>
	</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

</body>

</html>









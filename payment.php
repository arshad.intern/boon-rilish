<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Payment</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
	
		   <style>
		.donate-now {
     list-style-type:none;
     margin:25px 0 0 0;
     padding:0;
	 overflow:hidden;
}

.donate-now li {
     float:left;
     margin:0 5px 0 0;
    width:47%;
    height:auto;
}

.donate-now label, .donate-now input {
    display:block;
    top:0;
    left:0;
    right:0;
    bottom:0;
}

.donate-now input[type="radio"] {
    opacity:0.01;
    z-index:100;
}

.donate-now input[type="radio"]:checked + label,
.Checked + label {
    background:yellow;
}

.donate-now label {
     padding:50px;
	 text-align:center;
     border:1px solid #CCC; 
     cursor:pointer;
    z-index:90;
}

.donate-now label:hover {
     background:#fff;
	 box-shadow:3px 3px 25px #333;
	 border-radius:13px;
}
</style>

</head>

<body>
<?php include("inc/topmenu2.php"); ?>
<div class="container">
	<div class="row"><br>
			<div class="well rg_form">
				<center><h3>ORDER CONFIRMATION<span class="pull-right"><button type="button" onclick="window.history.go(-1);" class="btn btn-primary btn-sm">Back</button></span></h3></center>
				<hr/>

				<?php 
				if(isset($_GET['no_valid_card']))
				{
					echo '<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times</a>
							<p>This is not an valid card number</p>
						</div>';
				}
			?>

			<div class="row">
		<div class="col-md-12">
			<form role="form" name="payment_form" method="POST" action="payment_val.php">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="well">
							<div class="row">
								<div class="col-md-12">
									<?php
										include("admin/connection.php");
										$dd = mysqli_query($con, "SELECT * FROM `users` WHERE email = '$a_email'") or die(mysqli_error($con));
										$row = mysqli_fetch_array($dd);
									?>
									<h4>Delivery Address </h4>
									<p><input type="hidden" name="fullname" data-validation="required" value="<?php echo $row['name']; ?>"><?php echo $row['name']; ?><br>
									<input type="hidden" name="email" data-validation="required" value="<?php echo $row['email']; ?>"><?php echo $row['email']; ?><br>
									<input type="hidden" name="phone" data-validation="required" value="<?php echo $row['phone']; ?>"><?php echo $row['phone']; ?><br>
									<input type="hidden" name="address" data-validation="required" value="<?php echo $row['address']; ?>"><?php echo $row['address']; ?></p>
								</div>									
							</div>
						</div>
					</div>	<br><br>								
				   <div class="row">
			          	<div class="col-md-6">
		                	<div class="form-group"> 
			                	<label for="cardNumber">CARD HOLDER NAME</label>
			                	<input type="text" class="form-control input-lg myinput" data-validation="required" placeholder="Card Holder Name" name="holder_name" required autofocus />
							</div><br><br>
						</div>
													
					<div class="col-md-6">
						<div class="form-group">
							<label for="cardNumber">CARD NUMBER</label>
							<input type="text" class="form-control input-lg myinput" data-validation="required number length" data-validation-length="max16" placeholder="Valid Card Number" name="card_num" required autofocus />
						</div><br><br>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-7 col-md-7">
						<label for="expiryMonth">EXPIRY MONTH AND YEAR</label>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-6 col-lg-6 pl-ziro">
									<select data-validation="required" class="form-control input-lg myinput" name="expiry_month" placeholder="MM" required />
										<option value="">--SELECT--</option>
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									</div>
									<div class="col-xs-6 col-lg-6 pl-ziro">
										<input type="text" class="form-control input-lg myinput" data-validation="required" name="expiry_year" placeholder="YY" required />
									</div>
								</div>
							</div><br><br>
						</div>
							<div class="col-xs-5 col-md-5 pull-right">
								<div class="form-group">
								  <label for="cvCode">
								  CV CODE
								   </label>
								   <input type="password" class="form-control input-lg myinput" name="cvc" data-validation="required" data-validation-length="max3" placeholder="CV" required />
								</div>
							</div><br><br>
						</div>
							<?php
								include("admin/connection.php");

									$sql = mysqli_query($con, "SELECT SUM(total) AS total_price FROM `order`") or die(mysqli_error($con));
									$rr = mysqli_fetch_array($sql);
							?>
							<ul class="nav nav-pills nav-stacked" style="border-radius:0px;">
								<li class="active"><a href="#"><span class="badge pull-right">Rs. <?php echo $rr['total_price']; ?></span> Final Payment</a></li>
							</ul><br><br>
						<br/>

						<input type="submit" name="aa" value="Pay" class="btn btn-success btn-lg btn-block">
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

</body>

</html>

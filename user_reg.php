<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>USER REGISTRATION</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

</head>

<body>
	
	<div class="banner jarallax">
        <div class="agileinfo-dot">
            <div class="header">
                <div class="container-fluid">
                    <div class="header-left">
                        <div class="w3layouts-logo grid__item">
                            <h1>
                                <a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
                            </h1>
                        </div>
                    </div>
                    <div class="top-nav">
                        <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                	<ul class="nav navbar-nav">
                                	</ul>
                            	</div>
                            <div class="clearfix"> </div> 
   						</nav>      
                </div>      
            </div>
        </div>

<div class="container">
	<div class="row"><br>
		<div class="col-md-6 col-md-offset-3" style="margin:bottom:5%; ">
			<div class="well rg_form">
				<center><h3>USER REGISTERATION</h3></center>
				<hr/>
				<form method="POST" action="users_val.php">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" id="name" class="form-control" data-validation="required" placeholder="Enter Your Name" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Email ID</label>
								<input type="email" name="email" data-validation="required email" class="form-control" placeholder="Enter Your Email" required/>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Gender</label>
								<select class="form-control" data validation="required" name="gender">
									<option value="">--SELECT--</option>
									<option value="Male"> Male </option>
									<option value="Female"> Female </option>
								</select>					
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label>Phone</label>
								<input type="text" name="phone" data-validation="required number length" data-validation-length="max10" class="form-control" placeholder="Enter Your Phone" required>
							</div>
						</div>
					</div>
			
					<div class="form-group">
						<label>Address</label>
						<textarea rows="3" name="address" class="form-control" data-validation="required" placeholder="Enter Your Address" required></textarea>
					</div>
			
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" data-validation="required" class="form-control" placeholder="Enter Your Password" required>
					</div>
					
					<div class="form-group">
						<input type="submit" name="reg_btn" class="btn btn-primary btn-block" value="Register">
					</div>
				</form>
				<p>Already a Member? <a href="index.php">Login Here </a></p>
			</div>
		</div>
	</div>
</div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <?php include("inc/footer.php"); ?>

    <script>
            $( document ).ready(function() {
                $( "#name" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
        </script>

</body>

</html>

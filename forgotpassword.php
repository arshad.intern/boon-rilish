<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("inc/head.php"); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Feedback Form</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="css/font-awesome.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<div class="banner jarallax">
        <div class="agileinfo-dot">
            <div class="header">
                <div class="container-fluid">
                    <div class="header-left">
                        <div class="w3layouts-logo grid__item">
                            <h1>
                                <a class="link link--ilin" href="#"><span>BOON</span><span>RELISH</span></a>
                            </h1>
                        </div>
                    </div>
                    <div class="top-nav">
                        <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                	<ul class="nav navbar-nav">
                                	</ul>
                            	</div>
                            <div class="clearfix"> </div> 
   						</nav>      
                </div>      
            </div>
        </div>
<div class="container">
	<div class="row"><br>
		<div class="col-md-6 col-md-offset-3" style=" margin-top:10%; margin:bottom:30%; ">
			<div class="well rg_form">
					<div class="login-body">

                    <h1 class="text-center">Recover Password</h1>
                  
                    <br>
                    <br>
                    <br>

                    <form class="" method="POST">
                        <div class="input-group" style="text-align: center; margin: 10px auto;">
                            <input class="form-control input-lg" name="phone" 
                                placeholder="Enter Mobile Number" type="text">
                        </div>
					
						
						<div class="form-group" style="text-align: center; margin: 10px auto;">
                            <input type="submit" class="btn btn-lg btn-primary" name="submit_btn" value="Recover Password">
						</div>
                    </form>

                    <?php
                        if(isset($_POST['submit_btn']))
                        {
                            $phone = $_POST['phone'];
                            include("connection.php");
                            $ee = mysqli_query($con, "SELECT * FROM `users` WHERE phone = '$phone'") or die(mysqli_error($con));
                            $rr = mysqli_num_rows($ee);

                            $row = mysqli_fetch_array($ee);

                            if($rr > 0)
                            {
                                $ff = mysqli_fetch_array($ee);
                                include('way2sms-api.php');
                                $client = new WAY2SMSClient();
                                $client->login($wphone, $wpassword);
                                $client->send($wphone, 'Dear '.$row['name'].' your password to login is '.$row['password']);
                                
                                sleep(1);
                                $client->logout(); 
                                header("location:index.php");
                            }
                            else
                            {
                                echo '<script>
                                            alert("Number is not present in our database");
                                    </script>';

                            }

                            
                        }


                    ?>
		              </div>
					</div>
				</div>
			</div>
		</div><br><br><br><br><br><br><br><br><br>
		<!--footer-->
		<?php include("inc/footer.php"); ?>
        <!--//footer-->
	</div>
	<!-- Classie -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
	<script>
  		$.validate({
    	lang: 'en'
  		});
	</script>
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>

</body>
</html>